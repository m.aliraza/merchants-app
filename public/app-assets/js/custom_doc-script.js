$(document).ready(function(){
    $('#item-select-all').on('click', function () {
        if ($(this).is(":checked")) {
            $('.checkbox').prop('checked', true);
        } else {
            $('.checkbox').prop('checked', false);
        }
    });


    //Apply an action to the Users Table
    $('#apply_doc').on('click', function () {

        var ids = [];
        var action = $('#action_doc').val();
        var currentToken = $('meta[name="csrf-token"]').attr('content');
        var url = $(this).data('url');


        var errorBlock = $('.bulk-action-error');
        if ( action === "") {
            errorBlock.text('Please select any action')
            errorBlock.css('display', 'block')
            // alert("Please select any action");
            return false;
        } else if(action === 'send_email') {
            return false;
        }


        $('.checkbox').each(function () {
            if ($(this).is(":checked")) {
                ids.push($(this).val());
            }
        });

        if (ids.length === 0) {
            errorBlock.text('Nothing is selected.')
            errorBlock.css('display', 'block')
            return false;
        }

        $.ajax({
            type: "POST",
            data: {action: action, ids: ids, _token: currentToken},
            url: url,
            success: function (response) {
                console.log(response);
                if (response.status) {
                    window.location.href = response.redirectTo;
                } else {
                    // errorBlock.text('Something went wrong, Please try again.')
                    // errorBlock.css('display', 'block')
                }
            }
        });
    });

})
