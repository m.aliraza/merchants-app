<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/setting', 'HomeController@setting')->name('setting');

Route::get('/user', 'UserController@index')->name('user');
Route::get('/user/add', 'UserController@add')->name('user.add');
Route::post('/user/change_status', 'UserController@change_status')->name('user.change_status');
Route::get('/user/{id}', 'UserController@single')->name('user.single');
Route::post('/user/insert', 'UserController@insert')->name('user.insert');
Route::get('/user/edit/{id}', 'UserController@edit')->name('user.edit');
Route::post('/user/edit_insert', 'UserController@edit_insert')->name('user.edit_insert');
Route::post('users/action', 'UserController@action');
Route::post('users/send_bulk_emails', 'UserController@send_bulk_emails')->name('send.bulk.emails');


Route::get('/app', 'ApplicationController@index')->name('app');
Route::get('/app/add', 'ApplicationController@add')->name('app.add');
Route::get('/app/edit/{id}', 'ApplicationController@edit')->name('app.edit');
Route::post('/app/insert', 'ApplicationController@insert')->name('app.insert');
Route::post('/app/insert_edit', 'ApplicationController@insert_edit')->name('app.insert_edit');
Route::post('/app/add_media', 'ApplicationController@add_media')->name('app.add_media');
Route::get('/app/{id}', 'ApplicationController@single');
Route::post('app/action', 'ApplicationController@action');
Route::post('app/add_comment', 'ApplicationController@add_comment')->name('app.add_comment');



Route::get('/doc', 'DocumentManagerController@index')->name('doc');
Route::get('/doc/add', 'DocumentManagerController@add')->name('doc.add');
Route::post('/doc/insert', 'DocumentManagerController@insert')->name('doc.insert');
Route::post('doc/action', 'DocumentManagerController@action');

Route::get('import_export/', 'HomeController@import_export');
Route::post('export/users', 'HomeController@export_users')->name('user.export');
Route::post('import/users', 'HomeController@import_users')->name('user.import');

Route::post('export/app', 'HomeController@export_app')->name('app.export');
Route::post('import/app', 'HomeController@import_app')->name('app.import');

Route::middleware(['auth'])->group(function () {
    Route::post('/doc/create/folder', 'DocumentManagerController@createNewFolder')->name('create.folder');
    Route::post('/doc/get/sub/folders', 'DocumentManagerController@getSubFolders')->name('get.subfolders');
    Route::post('/doc/delet/folders', 'DocumentManagerController@deleteFolders')->name('delete.folder');
    Route::post('/doc/delet/file', 'DocumentManagerController@deleteFile')->name('delete.file');

    Route::post('/doc/upload', 'DocumentManagerController@uploadDocument')->name('document.upload');

    Route::get('/doc/download/{id}', 'DocumentManagerController@downloadDocument')->name('document.download');
});
