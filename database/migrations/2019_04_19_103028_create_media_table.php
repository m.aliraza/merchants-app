<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('real_name',250)->nullable();
            $table->string('title',250)->nullable();
            $table->text('comment')->nullable();
            $table->string('name',250)->nullable();
            $table->string('base_url',250);
            $table->string('extension',250);
            $table->string('size',250);
            $table->enum('doc_type',[1,2])->comment('1=normal, 2=doc_manager')->default(1);
            $table->enum('status',[1,2])->comment('1=active, 2=in-active')->default(1);
            $table->string('document_status')->comment('custom_comment')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
