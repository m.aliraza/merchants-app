<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mid')->nullable();
            $table->string('dba')->nullable();
            $table->string('contact')->nullable();
            $table->string('legal_name')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('email')->nullable();
            $table->string('address_1')->nullable();
            $table->string('phone_no')->nullable();
            $table->text('open_date')->nullable();
            $table->string('address_2')->nullable();
            $table->string('postal_code')->nullable();
            $table->enum('status',[1,2,3,4])->comment('1=created,2=active,3=closed,4=approved,5=not_approved')->default(2);
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('acquirer')->nullable();
            $table->timestamps();

            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
