<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,100) as $index) {
            \Illuminate\Support\Facades\DB::table('applications')->insert([
                'mid' => $faker->uuid,
                'dba' => $faker->buildingNumber,
                'contact' => $faker->firstName(),
                'email' => $faker->safeEmail,
                'address_1' => $faker->address,
                'address_2' => $faker->streetAddress,
                'postal_code' => $faker->postcode,
                'country' => $faker->country,
                'user_id' => $faker->numberBetween(1,50),
                'acquirer' => $faker->company,
                'status' => $faker->numberBetween(2,3),
                'created_by' => 1,
                'updated_by' => 1,
                'deleted_at'=> Null,
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
            ]);
        }

        foreach (range(1,1000) as $index) {
            \Illuminate\Support\Facades\DB::table('application_equipments')->insert([
                'application_id' => $faker->numberBetween(1,50),
                'terminal_id' => $faker->swiftBicNumber,
                'part_name' => $faker->userName,
                'serial_no' => $faker->randomNumber(),
                'item_type' => $faker->randomElement([
                    'rental',
                    'purchased',
                    'leased'
                ]),
                'item_status' => $faker->randomElement([
                    'active',
                    'inactive'
                ]),
                'created_by' => 1,
                'updated_by' => 1,
                'deleted_at'=> Null,
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
            ]);
        }
    }
}
