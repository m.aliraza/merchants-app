<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        foreach (range(1,50) as $index) {
            \Illuminate\Support\Facades\DB::table('users')->insert([
                'first_name' => $faker->firstName,
                'company_name' => $faker->company,
                'display_name' => $faker->name,
                'last_name' => $faker->lastName,
                'email' => $faker->safeEmail(),
                'phone_no' => $faker->phoneNumber,
                'role' => $faker->randomElement([
                    'admin',
                    'super_user',
                    'user'
                ]),
                'status' => $faker->numberBetween(2,3),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'created_by' => 1,
                'updated_by' => 1,
                'deleted_at'=> Null,
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
            ]);
        }
    }
}
