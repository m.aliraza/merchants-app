<?php

namespace App\Exports;

use App\Application;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ApplicationExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('applications')->whereNull('deleted_at')
            ->select('mid','acquirer','dba','legal_name','address_1','address_2','city','state','postal_code','country','contact','phone_no','email','user_id','open_date') 
            ->get();
    }

    public function headings(): array
    {
        return [
            'MID',
            'ACQUIRER',
            'DBA',
            'LEGAL NAME',
            'ADDRESS 1',
            'ADDRESS 2',
            'CITY',
            'PROVINCE',
            'POSTAL CODE/ZIP',
            'COUNTRY',
            'CONTACT',
            'PHONE NO',
            'E-MAIL',
            'USER/AGENT',
            'DATE OPEN'
        ];
    }
}
