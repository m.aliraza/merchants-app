<?php

namespace App\Exports;

use App\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection , WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('users')->whereNull('deleted_at')
            ->select('first_name','last_name','display_name','phone_no','email','role')
            ->get();
    }

    public function headings(): array
    {
        return [
            'First Namae',
            'Last Name',
            'Contact Name',
            'Telephone',
            'Email',
            'Role'
        ];
    }
}
