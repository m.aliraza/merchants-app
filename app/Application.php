<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Directory;
use App\Media;

class Application extends Model{

    use Notifiable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mid', 'legal_name', 'dba', 'address_1', 'city', 'state', 'country', 'contact', 'phone_no', 'open_date', 'email', 'address_2', 'postal_code', 'user_id', 'acquirer' , 'created_by', 'updated_by','created_at','updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array  Equipment
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // get sorted application array
    public static function get_sorted_array($equipments){

        try{
            $equipment_array = [];
            $counter = 0;
            $new_array_counter = 0;
            foreach ($equipments as $equipment){
                if (isset($equipment['terminal_id'])){
                    $equipment_array[$new_array_counter]['terminal_id'] = $equipment['terminal_id'];
                }elseif (isset($equipment['part_name'])){
                    $equipment_array[$new_array_counter]['part_name'] = $equipment['part_name'];
                }elseif( isset($equipment['serial_no'])){
                    $equipment_array[$new_array_counter]['serial_no'] = $equipment['serial_no'];
                }elseif ( isset($equipment['item_type']) ) {
                    $equipment_array[$new_array_counter]['item_type'] = $equipment['item_type'];
                }elseif (isset($equipment['item_status'])){
                    $equipment_array[$new_array_counter]['item_status'] = $equipment['item_status'];
                }

                $counter++;
                if ($counter == 5){
                    $new_array_counter++;
                    $counter = 0;
                }
            }

            return $equipment_array;
        }catch (\Exception $exception){
            return ['status' => false, 'error' => $exception->getMessage()];
        }
    }

    //get application equipments
    public function equipments()
    {
        return $this->hasMany(ApplicationEquipment::class, 'application_id','id');
    }

    // add media_object relationship
    public function user()
    {
        return $this->hasOne(User::class, 'id','user_id');
    }
    public function super_user(){
        return $this->hasOne(User::class, 'id','user_id');
    }

    //get application media
    public static function get_application_docs($application_id){

        $docs = DB::table('application_media')
            ->join('media', 'media.id', '=', 'application_media.media_id')
            ->select('media.*', 'application_media.application_id')
            ->where([
                'application_media.application_id' => $application_id,
                'application_media.deleted_at' => null,
                'media.deleted_at' => null
            ])
            ->get();

        return $docs;
    }

    // delete application
    public static function  delete_application($application_id){
        try {
            $application = Application::where('id', $application_id)->first();
            $mid    = $application->mid;
            // delete application



		try {
			ApplicationEquipment::where('application_id', $application_id)->delete();
                } catch (\Exception $e) {
                }

		try {
			// delete application media
                      ApplicationMedia::where('application_id', $application_id)->delete();
                } catch (\Exception $e) {

                }



		  $directory = Directory::where('name', $mid)->first();
		  if ($directory) {
			$directory->delete();
                  }



		try {
                  Media::where('base_directory_id', $directory->id)->delete();
                } catch (\Exception $e) {
                }


            $application->delete();
            return true;
        } catch (\Exception $e) {
echo 'bilal';exit;
            throw $e;
        }

    }

    // get application comments
    public static function get_application_comments($application_id) {
        $comments = DB::table('application_comments')
            ->join('users', 'application_comments.user_id', '=', 'users.id')
            ->select('application_comments.*','users.display_name')
            ->where([
                'application_comments.application_id' => $application_id,
                'application_comments.deleted_at' => null,
                'users.deleted_at' => null
            ])
            ->orderBy('application_comments.id', 'desc')
            ->get();
        return $comments;
    }

}
