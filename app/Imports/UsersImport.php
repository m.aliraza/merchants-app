<?php

namespace App\Imports;

use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class UsersImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        try {
            return new User([
                'first_name' => $row[0],
                'last_name' => $row[1],
                'display_name' => $row[2],
                'phone_no' => $row[3],
                'email' => $row[4],
                'role' => $row[5],
                'password' => Hash::make(123)
            ]);
        } catch (\Exception $e) {
            //
        }

    }
    public function startRow(): int
    {

    }

}
