<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Media extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'media';

    protected $fillable = [
        'title','comment','doc_type','real_name','name','base_url', 'extension', 'size', 'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_at', 'base_directory_id','user_id',
    ];


    protected $hidden = [
        'created_at', 'updated_at',
    ];

    // add new media object
    public static function add($data){
        try{
            $new_media = new Media();
            $new_media->real_name = $data['real_name'];
            $new_media->name = $data['name'];
            $new_media->base_url = $data['base_url'];
            $new_media->extension = $data['extension'];
            $new_media->size = $data['size'];
            $new_media->created_by = Auth::id();
            $new_media->updated_by = Auth::id();
            $new_media->save();

            return $new_media;
        }catch (\Exception $exception){
            return Redirect::back()->with([
                'info' => 'Media Not added due to Technical Reasons, Please Try again later'
            ]);
        }

    }
    public function user(){
        return $this->belongsTo(User::class);
    }

}
