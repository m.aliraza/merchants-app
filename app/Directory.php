<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Directory extends Model
{
    protected $fillable = ['name', 'base_directory_id'];

    public function children()
    {
        return $this->hasMany(Directory::class, 'base_directory_id', 'id');
    }

    public function files()
    {
        return $this->hasMany(Media::class, 'base_directory_id', 'id');
    }
}
