<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserValidation extends Controller{

    //Add new user validation
    public static function add_edit($data = [], $type = 'add') {

        $rules = array(
            'first_name' => 'max:100',
            'last_name' => 'max:100',
            'display_name' => 'required|max:1000',
            'company_name' => 'required|max:250',
            'email' =>  'required|unique:users,email,NULL,id,deleted_at,NULL',
            'phone_no' =>  'required',
            'role' => 'required'
        );
        switch ($type) {
            case 'add':
                $rules['password'] = 'required';
                break;
            case 'update':
                $rules['email'] = 'unique:users,email,'.$data['id'];
                $rules['id'] = 'required|exists:users,id,deleted_at,NULL';
                break;
            default;
        }

        $validator = Validator::make($data, $rules);
        return $validator;
    }
}
