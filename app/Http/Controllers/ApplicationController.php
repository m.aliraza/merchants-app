<?php

namespace App\Http\Controllers;

use App\Supervisor;
use Storage;
use App\Application;
use App\ApplicationEquipment;
use App\ApplicationMedia;
use App\Directory;
use App\Helpers\Helper;
use App\Media;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;

class ApplicationController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }


    // get all applications
    public function index(Request $request){
	$is_search = false;
//        $applications = Application::with('user')->get();
        $applications = Application::with('user');
//        $sort = $applications->orderBy('dba','asc');
//       $sort=$sort->orderBy('dba','asc');
//        $sort1= $sort->get();
        // status search result
        if(!empty($request->get('status'))){
            $applications = $applications->Where('applications.status', $request->input('status'));
        }
//        // role base search
//        if(!empty($request->input('role'))){
//            $users = $users->Where('users.role', $request->input('role'));
//        }



        if(!empty($request->input('search_keyword'))){
	    $is_search = true;
            $search_keyword = $request->input('search_keyword');
            $applications->where(function($query) use ($search_keyword){
                $query->where('mid','LIKE', '%'. $search_keyword . '%');
                $query->orWhere('dba','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('contact','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('email','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('address_1','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('address_2','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('country','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('postal_code','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('acquirer','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('city','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('state','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('country','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('open_date','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('legal_name','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('phone_no','LIKE', '%'. $search_keyword. '%');
            });
        }
        $role = auth()->user()->role;

        if ($role == 'user') {
            $applications->where('user_id', auth()->user()->id);
        }
	if ($role == 'super_user') {
        $user_ids = array();
            $qry = DB::table('users')->join('supervisors','supervisors.user_id','=','users.id')
                ->select('supervisors.id','users.first_name','users.last_name')
                ->where('supervisor_id','=',auth()->user()->id)->get()->toArray();

//
//		$supervisee = DB::table('supervisors')
//                     ->select(DB::raw('user_id'))
//                     ->where('supervisor_id', '=', auth()->user()->id)
//                     ->get()->toArray();
//		foreach ($qry as $user) {
//	   		$user_ids[] = $user->user_id;
//                }
//            $applications->whereIn('user_id', $user_ids);

        }
        if ($request->input('user_id')) {
            $applications->where('user_id', $request->input('user_id'));

//            $data = $applications->where('dba','LIKE', '%'. "zeshan".'%')->get();
//            dd($data)->toSql();
//            $data = Application::all();
//            dd($data);
//            dd($request->input('user_id'));
        }
        if ($request->input('sort')) {
            $applications->orderBy('dba', $request->input('sort'));
        } else {
                  $applications->orderBy('dba', 'asc');
        }
        if ($request->input('page')) {
              $applications->offset($request->input('page'));
        }
        $applications = $applications->paginate(25);
        $users = User::all();
        $supervisor = Supervisor::all();
        if($role == 'user'){
            return view('application/index')->with(['data' => $applications, 'users' => $users]);
        }
        elseif ($role == 'admin'){
            return view('application/index')->with(['data' => $applications, 'users' => $users]);
        }
        elseif($role == 'super_user'){
            return view('application/index')->with(['data' => $applications, 'users' => $users,'qry' => $qry]);
        }
//        return view('application/index')->with(['data' => $applications, 'users' => $users]);
//        return view('application/index',compact('applications','users','qry'));
    }

    // add application view
    public function add(){
        $role = auth()->user()->role;
        if ($role != 'admin') {
            return redirect('app');
        }
        $users = User::all();
        return view('application/add')->with('users' , $users);
    }

    // insert application in database
    public function insert(Request $request){
        //validate user request
        $validator = ApplicationValidation::add_edit($request->all(), 'add');
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            DB::beginTransaction();
            try{
                $application_object = $this->addOrUpdate($request->all());
                $application_equipments = Application::get_sorted_array($request->input('equipment'));

//                ApplicationEquipment::create($application_equipments);

                foreach ($application_equipments as $equipment){

                    $insert_application_equipment = [
                        'application_id' => $application_object->id,
                        'created_by' => Auth::id(),
                        'updated_by' => Auth::id(),
                        'terminal_id' => $equipment['terminal_id'],
                        'part_name' => $equipment['part_name'],
                        'serial_no' => $equipment['serial_no'],
                        'item_type' => $equipment['item_type'],
                        'item_status' => $equipment['item_status'],
                    ];

                    // insert applicationwe equipments
                    ApplicationEquipment::create($insert_application_equipment);
                }
                $this->createApplicationFolder($request);

                try {
                    if ($request->file('pic')) {
                        if (!is_dir('public/'.auth()->user()->id)) {
                            $base_path = 'public/'.auth()->user()->id .'-'.date('d-m-Y');
                            Storage::makeDirectory($base_path);
                        } else {
                            $base_path = 'public/'.auth()->user()->id .'-'.date('d-m-Y');
                        }

                        $directory_path = $directory_path = 'app/'.$base_path;

                        $pic_name = Helper::random_time().'app_doc'.'.'.$request->file('pic')->getClientOriginalExtension();
                        $destination_path =  storage_path($directory_path);
                        $request->file('pic')->move($destination_path, $pic_name);
                        $url = str_replace('public', '', $base_path);
                        $pic_object = [
                            'real_name' => $request->file('pic')->getClientOriginalName(),
                            'name' => $pic_name,
                            'base_url' => $url,
                            'size'   => 1234444,
                            'extension' => $request->file('pic')->getClientOriginalExtension(),
                            'document_status' => $request->input('document_status'),
                            'doc_type' => 2,
                            'base_directory_id' => $request->input('app_directory_id'),
                            'user_id' => Auth::id()
                        ];

                        //add in database
                        Media::create($pic_object);
                    }

                } catch (\Exception $e) {

                }

                DB::commit();
                return Redirect::route('home')->with([
                    'info' => 'Application Added Successfully'
                ]);

            }catch (\Exception $exception){

                DB::rollBack();
                Redirect::back()->with([
                    'info' => $exception->getMessage()
                ]);
            }
        }
    }

    // add application view
    public function edit($id){
        $role = auth()->user()->role;
        if ($role != 'admin') {
            return redirect('app');
        }
        $application_object = Application::with('equipments')->find($id);
        $users = User::all();
        return view('application/edit')->with([
            'application_object' => $application_object,
            'users' => $users,
        ]);
    }

    // insert edit application in database
    public function insert_edit(Request $request){

        //validate user request
        $validator = ApplicationValidation::add_edit($request->all(), 'update');
        if ($validator->fails()) {
//            dd($validator);
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            DB::beginTransaction();
            try{

                $application_object = $this->addOrUpdate($request->all());
                Log::info('application_object is updated');
                Log::info($application_object);

                // remove previous equipments
                ApplicationEquipment::where('application_id', $request->input('id'))->delete();

                $application_equipments = Application::get_sorted_array($request->input('equipment'));

                foreach ($application_equipments as $equipment){

                    $insert_application_equipment = [
                        'application_id' => $request->input('id'),
                        'created_by' => Auth::id(),
                        'updated_by' => Auth::id(),
                        'terminal_id' => $equipment['terminal_id'],
                        'part_name' => $equipment['part_name'],
                        'serial_no' => $equipment['serial_no'],
                        'item_type' => $equipment['item_type'],
                        'item_status' => $equipment['item_status'],
                    ];

                    // insert application equipments
                    ApplicationEquipment::create($insert_application_equipment);
                }

                DB::commit();
                return Redirect::back()->with([
                    'info' => 'Application Updated Successfully'
                ]);

            }catch (\Exception $exception){

                DB::rollBack();
                Redirect::back()->with([
                    'info' => $exception->getMessage()
                ]);
            }
        }
    }

    // get single view
    public function single($id){

        try{
            $application = Application::with('equipments','user')->find($id);
            $application->doc = Application::get_application_docs($application->id);
            $application->comments = Application::get_application_comments( $application->id );

            return view('application/single')->with([
                'application' => $application
            ]);
        }catch (\Exception $exception){
            return view('application/single')->with([
                'info' => $exception->getMessage()
            ]);

        }

    }

    // upload application media
    public function add_media(Request $request){

        //validate user request
        $validator = ApplicationValidation::add_media($request->all(), 'add');
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            DB::beginTransaction();
            try{

                $pic_name = Helper::random_time().'app_doc'.'.'.$request->file('pic')->getClientOriginalExtension();
                $destination_path =  public_path('media/app');
                $request->file('pic')->move($destination_path, $pic_name);

                $pic_object = [
                    'real_name' => $request->file('pic')->getClientOriginalName(),
                    'name' => $pic_name,
                    'base_url' => url('media/app'),
                    'extension' => $request->file('pic')->getClientOriginalExtension(),
                    'size' => 1234,
                    'doc_type' => 2
                ];

                //add in database
                $media_object = Media::add($pic_object);

                $application_media = [
                    'application_id' => $request->input('application_id'),
                    'media_id' => $media_object->id,
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id()
                ];

//                dd($application_media);

                ApplicationMedia::create($application_media);

                DB::commit();
                return Redirect::back()->with([
                    'info' => 'Document Uploaded Successfully'
                ]);

            }catch (\Exception $exception){

                DB::rollBack();
                Redirect::back()->with([
                    'info' => $exception->getMessage()
                ]);
            }
        }
    }

    // add or update
    private function addOrUpdate($data) {

        if(isset($data['mid']))
            $data['mid'] =  $data['mid'];
        if(isset($data['dba']))
            $data['dba'] =  $data['dba'];
        if(isset($data['contact']))
            $data['contact'] =  $data['contact'];
        if(isset($data['email']))
            $data['email'] =  $data['email'];
        if(isset($data['address_1']))
            $data['address_1'] =  $data['address_1'];
        if(isset($data['address_2']))
            $data['address_2'] =  $data['address_2'];
        if(isset($data['country']))
            $data['country'] =  $data['country'];
            if(isset($data['postal_code']))
                $data['postal_code'] =  $data['postal_code'];
        if(isset($data['user_id']))
            $data['user_id'] =  $data['user_id'];
        if(isset($data['acquirer']))
            $data['acquirer'] =  $data['acquirer'];
        if(isset($data['city']))
            $data['city'] =  $data['city'];
        if(isset($data['state']))
            $data['state'] =  $data['state'];
        if(isset($data['phone_no']))
            $data['phone_no'] =  $data['phone_no'];
        if(isset($data['legal_name']))
            $data['legal_name'] =  $data['legal_name'];
        if(isset($data['open_date']))
            $data['open_date'] =  $data['open_date'];

        $data['updated_by'] = Auth::id();
        $data['created_by'] = Auth::id();

        return Application::updateOrCreate(['id' => (isset($data['id']) ? $data['id'] : NULL)], $data);
    }

    // action for applications
    public function action(Request $request) {

        $action = $request->input('action');

        DB::beginTransaction();
        try{
            if($request->has('ids')) {
                $ids = $request->input('ids');
                for($i = 0; $i < count($ids); $i++) {
                    $applications = Application::where('id', $ids[$i]);

                    switch ($action) {
                        case 'delete':
                            //add code to delete complete application
                            Application::delete_application($ids[$i]);
                            break;
                        case 'active':
                            $applications->update(['status' => 2]);
                            break;
                        case 'closed':
                            $applications->update(['status' => 3]);
                            break;
			            case 'pending':
                            $applications->update(['status' => 1]);
                            break;
                    }
                }
            }

            Session::flash('success', "Application".  ucfirst($action) . " successfully");
            DB::commit();
            if ($request->has('home_page') || !empty($request->input('home_page'))){
                return response(['status' => true, 'message' => 'Application '. ucfirst($action) . ' successfully', 'redirectTo' => url('/home')]);
            }else{
                return response(['status' => true, 'message' => 'Application'. ucfirst($action) . ' successfully', 'redirectTo' => url('/app')]);
            }

        } catch (\Exception $ex) {
            Session::flash('error', "Something went wrong");
            DB::commit();
            return response(['status' => false, 'message' => 'something went wrong', 'error' => $ex->getMessage()]);
        }

    }

    // add application comments
    public function add_comment(Request $request){

        DB::table('application_comments')->insert([
            'application_id' => $request->input('application_id'),
            'comment' => htmlentities($request->input('comment')),
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);

	try {
		$application   = Application::where('id', $request->input('application_id'))->first();
                $email          = 'info@universalpayments.ca';
                $subject        = auth()->user()->first_name . ' ' . auth()->user()->last_name .' commented on '. $application->dba;
                $message        = auth()->user()->first_name . ' ' . auth()->user()->last_name .' commented on '. $application->dba . ':    ' . strip_tags($request->input('comment'));

                Mail::send('emails.bulk', ['data' => ['message' => $message] ], function ($m) use ($email, $subject)
                {
                  $m->from('bilal.imdad@gmail.com', 'Universal Payments');
                  $m->to($email);
                  $m->subject($subject);
                });
        } catch (\Exception $e) {

	}
        return back()->with('info', 'comment added successfully');
    }

    public function createApplicationFolder($request) {
        $baseAppDirectory = Directory::where('is_merchants_directory', 1)->first();
        if (empty($baseAppDirectory)) {
            $directory = new Directory;
            $directory->name = 'Merchants';
            $directory->base_directory_id =  null;
            $directory->path = 'public/merchants';
            $directory->is_merchants_directory = 1;
            Storage::makeDirectory('public/merchants');
            $directory->save();
            $baseAppDirectory = Directory::where('is_merchants_directory', 1)->first();
        }
        $directory_path = 'public/merchants/'. $request->mid;
        $directory = new Directory;
        $directory->name = $request->mid;
        $directory->user_id = $request->user_id;
        $directory->base_directory_id =  $baseAppDirectory->id;
        $directory->path = $directory_path;
        $directory->save();
        $request->merge(['app_directory_id' => $directory->id]);
        $request->merge(['app_directory_path' => $directory_path]);
        Storage::makeDirectory($directory_path);
    }

}
