<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Validation\PaymentValidation;
use App\Media;
use App\Models\Billing\PaymentPaidBy;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Application;
use App\ApplicationEquipment;
use App\ApplicationMedia;
use App\Supervisor;
use App\Directory;
use Storage;

class UserController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    // get all users
    public function index(Request $request){

//        $users = User::with('media_object')->where('role' ,'!=', 'admin');
        $users = User::with('media_object');

        // status search result
        if(!empty($request->get('status'))){
            $users = $users->Where('users.status', $request->input('status'));
        }

        // role base search
        if(!empty($request->input('role'))){
            $users = $users->Where('users.role', $request->input('role'));
        }

        if(!empty($request->input('search_keyword'))){
            $search_keyword = $request->input('search_keyword');

            $users->where(function($query) use ($search_keyword){
                $query->where('first_name','LIKE', '%'. $search_keyword . '%');
                $query->orWhere('last_name','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('display_name','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('company_name','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('phone_no','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('email','LIKE', '%'. $search_keyword. '%');
            });
        }

        $role = auth()->user()->role;
        if ($role != 'admin') {
            $users->where('id', auth()->user()->id);
        }
        $users = $users->orderBy('first_name', 'asc');
        $users = $users->get();
        return view('user/index')->with('users', $users);
    }

    // show add user view
    public function add(){
        $role = auth()->user()->role;
        if ($role != 'admin') {
            return redirect('user');
        }
        $users = User::where('role' ,'!=', 'admin')->where('role',  '!=', 'super_user')->get();
        return view('user/add')->with('users', $users);
    }

    // insert user row
    public function insert(Request $request){

        //validate user request
        $validator = UserValidation::add_edit($request->all(), 'add');
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            DB::beginTransaction();
            try{
                if (!is_null($request->file('pic'))) {
                    try {
                        $pic_name = Helper::random_time().'user_pic';
                        $destination_path =  public_path('media/user');
                        $request->file('pic')->move($destination_path, $pic_name);

                        $pic_object = [
                            'real_name' => $request->file('pic')->getClientOriginalName(),
                            'name' => $pic_name,
                            'base_url' => url('media/user'),
                            'extension' => $request->file('pic')->getClientOriginalExtension(),
                            'size' => '123'
                        ];

                        //add in database
                        $media_object = Media::add($pic_object);
                        $media_id = $media_object->id;

                    } catch (\Exception $e) {
                        DB::rollBack();
                        return Redirect::back()->withErrors($validator)->withInput()->with([
                            'info' => $e->getMessage()
                        ]);
                    }
                }else{
                    $media_id  = null;
                }

                // add new row
                $user = $this->addOrUpdate($request->all(), $media_id);

                $baseUsersDirectory = Directory::where('is_users_directory', 1)->first();
                if (empty($baseUsersDirectory)) {
                    $directory = new Directory;
                    $directory->name = 'Users';
                    $directory->base_directory_id =  null;
                    $directory->path = 'public/users';
                    $directory->is_users_directory = 1;
                    Storage::makeDirectory('public/users');
                    $directory->save();
                    $baseUsersDirectory = Directory::where('is_users_directory', 1)->first();
                }

                $path = 'public/users'.$user->id .'-'.date('d-m-Y');
                $userDirectory = new Directory;
                $userDirectory->name = $request->input('first_name'). ' ' . $request->input('last_name');
                $userDirectory->base_directory_id =  $baseUsersDirectory->id;
                $userDirectory->path = $path;
                $userDirectory->user_id = $user->id;
                Storage::makeDirectory($path);
                $userDirectory->save();

                Storage::makeDirectory($path);
                $data['base_directory'] = $baseUsersDirectory->id;

				if ($request->input('role') == 'super_user') {
					$user_ids = $request->input('user_ids');
					foreach ($user_ids as $user_id) {
						$supervisor_array = [
                            'user_id'    => $user_id,
							'supervisor_id' => $user->id
                        ];
						Supervisor::create($supervisor_array);
						$supervisor_array = array();
					}

				}
                DB::commit();
                return Redirect::route('user')->with([
                    'info' => 'User Added Successfully'
                ]);

            }catch (\Exception $exception){
                DB::rollBack();
                Redirect::back()->with([
                    'info' => $exception->getMessage()
                ]);
            }
        }
    }

    // edit view
    public function edit($id){
        $user_object = User::with('media_object')->find($id);
	$user = auth()->user();
        $role = $user->role;
        if ($role != 'admin' && $id != $user->id) {
            return redirect('user');
        }

	$supervisee = array();
	if ($user_object->role == 'super_user') {
           	$supervisee = DB::table('supervisors')
                     ->select(DB::raw('user_id'))
                     ->where('supervisor_id', '=', $id)
                     ->get()->toArray();
        }
	$user_ids = array();
	foreach ($supervisee as $user) {
	   $user_ids[] = $user->user_id;
        }
	$users = User::where('role' ,'!=', 'admin')->where('role',  '<>', 'super_user')->get();

        return view('user/edit')->with(['user_object' => $user_object, 'user_ids' => $user_ids, 'users' => $users]);
    }

    // edit insert
    public function edit_insert(Request $request){
        //validate user request
        $validator = UserValidation::add_edit($request->all(), 'update');
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            DB::beginTransaction();
            try{
                if (!is_null($request->file('pic'))) {
                    try {
                        $pic_name = Helper::random_time(). '.png';
                        $destination_path =  public_path('media/user');
                        $request->file('pic')->move($destination_path, $pic_name);

                        $pic_object = [
                            'real_name' => $request->file('pic')->getClientOriginalName(),
                            'name' => $pic_name,
                            'base_url' => url('media/user'),
                            'extension' => $request->file('pic')->getClientOriginalExtension(),
                            'size' => '123'
                        ];

                        //add in database
                        $media_object = Media::add($pic_object);
                        $media_id = $media_object->id;

                    } catch (\Exception $e) {
                        DB::rollBack();
                        return Redirect::back()->withErrors($validator)->withInput()->with([
                            'info' => $e->getMessage()
                        ]);
                    }
                }else{
                    $media_id  = null;
                }

                // add new row
                $this->addOrUpdate($request->all(), $media_id);
				if ($request->input('role') == 'super_user') {
					DB::statement('Delete from supervisors where supervisor_id = ?', [$request->input('id')]);
					$user_ids = $request->input('user_ids');
					foreach ($user_ids as $user_id) {
						$supervisor_array = [
                            'user_id'    => $user_id,
							'supervisor_id' => $request->input('id')
                        ];
						Supervisor::create($supervisor_array);
						$supervisor_array = array();
					}

				}

                DB::commit();
                return Redirect::back()->with([
                    'info' => 'User updated Successfully'
                ]);

            }catch (\Exception $exception){
			echo $exception->getLine();
echo $exception->getMessage();exit;
                DB::rollBack();
                Redirect::back()->with([
                    'info' => $exception->getMessage()
                ]);
            }
        }
    }

    // get single user
    public function single($id){
        $user_object = User::with('media_object')->find($id);
        return view('user/single')->with('user_object',$user_object);
    }

    // get single user
    public function change_status(Request $request){

        return Redirect::back()->with([
            'info' => 'Action Performed Successfully!'
        ]);

        dd($request->input('check_ids'));
        $user_object = User::with('media_object')->find($id);
        return view('user/single')->with('user_object',$user_object);
    }


    private function addOrUpdate($data, $media_id =  null) {

        try {
            if(isset($data['first_name']))
            $data['first_name'] =  $data['first_name'];
        if(isset($data['last_name']))
            $data['last_name'] =  $data['last_name'];
        if(isset($data['display_name']))
            $data['display_name'] =  $data['display_name'];
        if(isset($data['phone_no']))
            $data['phone_no'] =  $data['phone_no'];
        if(isset($data['email']))
            $data['email'] =  $data['email'];
        if(isset($data['role']))
            $data['role'] =  $data['role'];
        if(isset($data['status']))
            $data['status'] =  $data['status'];
        if(isset($data['password']))
            $data['password'] =  Hash::make($data['password']);


        if(!empty($media_id) || !is_null($media_id) )
            $data['media_id'] =  $media_id;

        $data['updated_by'] = Auth::id();
        $data['created_by'] = Auth::id();


        return User::updateOrCreate(['id' => (isset($data['id']) ? $data['id'] : NULL)], $data);
        } catch (\Exception $e) {
            throw $e;
        }

    }



    public function action(Request $request) {

        $action = $request->input('action');

        try{
            if($request->has('ids')) {
                $ids = $request->input('ids');
                for($i = 0; $i < count($ids); $i++) {
                    $user = User::where('id', $ids[$i]);

                    switch ($action) {
                        case 'created':
                            $user->update(['status' => 1]);
                            break;
                        case 'delete':
                            $user->delete();
                            break;
                        case 'active':
                            $user->update(['status' => 2]);
                            break;
                        case 'inactive':
                            $user->update(['status' => 3]);
                            break;
                    }
                }
            }

            Session::flash('success', "Users ".  ucfirst($action) . " successfully");
            return response(['status' => true, 'message' => 'Users '. ucfirst($action) . ' successfully', 'redirectTo' => url('/user')]);
        } catch (\Exception $ex) {
            Session::flash('error', "Something went wrong");
            return response(['status' => false, 'message' => 'something went wrong', 'error' => $ex->getMessage()]);
        }

    }

    function send_bulk_emails(Request $request) {


        if(empty($request->input('message'))) {
            Session::flash('showModal', true);
            return Redirect::back()->withInput();
        }

        $emails = [];
        $users = explode(',', $request->input('users'));

        $message = $request->input('message');

        for($i = 0; $i < count($users); $i++) {
            $emails[] = User::where('id', $users[$i])->value('email');
        }

        $subject = $request->input('email_subject');

        Mail::send('emails.bulk', ['data' => ['message' => $message] ], function ($m) use ($emails, $subject)
        {
            $m->from('LibraTechPeak@Gmail.com', 'Universal Payments');
            $m->to( $emails);
            $m->subject($subject);
        });

        Session::flash('success', 'Email sent successfully');
        return Redirect::back();
    }
}
