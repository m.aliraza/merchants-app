<?php

namespace App\Http\Controllers;

use App\Application;
use App\Exports\ApplicationExport;
use App\Exports\UsersExport;
use App\Helpers\Helper;
use App\Imports\ApplicationImport;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $active_applications    = Application::where('status',2);
        $closed_applications    = Application::where('status',3);
        $applications           = Application::with('user');

        // status search result
        if(!empty($request->get('status'))){
            $applications = $applications->Where('applications.status', $request->input('status'));
        }

        if(!empty($request->input('search_keyword'))){
            $search_keyword = $request->input('search_keyword');

            $applications->where(function($query) use ($search_keyword){
                $query->where('mid','LIKE', '%'. $search_keyword . '%');
                $query->orWhere('dba','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('contact','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('email','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('address_1','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('address_2','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('country','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('postal_code','LIKE', '%'. $search_keyword. '%');
                $query->orWhere('acquirer','LIKE', '%'. $search_keyword. '%');
            });
        }

        $role = auth()->user()->role;
	$user_id  = auth()->user()->id;
	if ($role == 'super_user') {
           $user_ids = array();
		$supervisee = DB::table('supervisors')
                     ->select(DB::raw('user_id'))
                     ->where('supervisor_id', '=', auth()->user()->id)
                     ->get()->toArray();
		foreach ($supervisee as $user) {
	   		$user_ids[] = $user->user_id;
                }
            $applications->whereIn('user_id', $user_ids);
	    $active_applications->whereIn('user_id', $user_ids);
            $closed_applications->whereIn('user_id', $user_ids);
        } else if ($role == 'user') {
		$applications->where('user_id', $user_id);
	        $active_applications->where('user_id', $user_id);
                $closed_applications->where('user_id', $user_id);
        }
        $active_applications = $active_applications->count();
        $closed_applications = $closed_applications->count();
	if ($request->input('sort')) {
		$applications->orderBy('dba', $request->input('sort'));
	} else {
	          $applications->orderBy('id', 'desc');
	}
        $applications = $applications
            ->limit(20)
            ->get();

        return view('home')->with([
            'applications'=> $applications,
            'active_applications' => $active_applications,
            'closed_applications' => $closed_applications
        ]);
    }

    public function setting()
    {
        return view('setting');
    }

    // import and export view
    public function import_export(){

        return view('import_export');
    }

    // export use data
    public function export_users(){
        return Excel::download(new UsersExport, Helper::random_time().'_user_list.xlsx');
    }

    // import and export view
    public function import_users(){
        try{
            Excel::import(new UsersImport(), request()->file('excel_file'));
            return back()->with('info', 'Data Import Successfully');
        } catch (\Exception $exception) {
            return back()->with('info', 'Data Import Successfully');
        }
    }

    // export applications
    public function export_app(){
        return Excel::download(new ApplicationExport(), Helper::random_time().'_applications.xlsx');
    }

    //import applications
    public function import_app(){
        try{
            Excel::import(new ApplicationImport(), request()->file('excel_file'));
            return back()->with('info_app', 'Data Import Successfully');

        }catch (\Exception $exception){
            return back()->with('info_app', $exception->getMessage());
        }
    }
}
