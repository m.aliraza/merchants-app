<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApplicationValidation extends Controller{

    //Add new user validation
    public static function add_edit($data = [], $type = 'add') {

        $rules = array(
            'mid' => 'required|max:100',
            'dba' => 'required|max:100',
            'contact' => 'required|max:1000',
            'email' =>  'required|unique:users,email,NULL,id,deleted_at,NULL',
            'address_1' =>  'required',
            'country' => 'required',
            'postal_code' => 'required',
            'user_id' => 'required',
            'acquirer' => 'required'
        );

        switch ($type) {
            case 'add':
//                $rules['password'] = 'required';
                break;
            case 'update':
                $rules['email'] = 'unique:users,email,'.$data['id'];
                $rules['id'] = 'required|exists:applications,id,deleted_at,NULL';
                break;
            default;
        }

        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function add_media($data = [], $type = 'add') {

        $rules = array(
            'pic' => 'required',
        );

        switch ($type) {
            case 'add':
//                $rules['password'] = 'required';
                break;
            case 'update':
//                $rules['email'] = 'unique:users,email,'.$data['id'];
//                $rules['id'] = 'required|exists:applications,id,deleted_at,NULL';
                break;
            default;
        }

        $validator = Validator::make($data, $rules);
        return $validator;
    }
}
