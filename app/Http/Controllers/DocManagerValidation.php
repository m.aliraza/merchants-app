<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DocManagerValidation extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Add new user validation
    public static function add_edit($data = [], $type = 'add') {

        $rules = array(
            'pic' => 'mimes:jpeg,jpg,png,gif,pdf,xls,xlsx,doc,docx|required|max:20000' // max 10000kb
        );
        switch ($type) {
            case 'add':
                break;
            case 'update':
//                $rules['email'] = 'unique:users,email,'.$data['id'];
//                $rules['id'] = 'required|exists:users,id,deleted_at,NULL';
                break;
            default;
        }

        $validator = Validator::make($data, $rules);
        return $validator;
    }
}
