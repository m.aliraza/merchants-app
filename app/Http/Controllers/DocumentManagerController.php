<?php

namespace App\Http\Controllers;

use Storage;
use App\Helpers\Helper;
use App\Media;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Directory;
use Illuminate\Support\Facades\Auth;
use App\Application;

class DocumentManagerController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = auth()->user();
        $files = [];
        $data = $this->getRootFilesAdnFolder($user, 0, $request);
        $files = $data['files'];
        $folders = $data['folders'];
        $root = 0;
        return view('doc/folder', compact('folders', 'root', 'files', 'user'));
    }

    // add view
    public function add()
    {
        return view('doc/add');
    }

    public function insert(Request $request)
    {
        $root = $request->input('root') == 'root' ?  : null ;
        //validate user request
        $validator = DocManagerValidation::add_edit($request->all(), 'add');
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            DB::beginTransaction();
            try {

                $pic_name = Helper::random_time(). '.' .$request->file('pic')->getClientOriginalExtension();
                $destination_path =  public_path('media/doc');
                $request->file('pic')->move($destination_path, $pic_name);

                $pic_object = [
                    'real_name' => $request->file('pic')->getClientOriginalName(),
                    'name' => $pic_name,
                    'base_url' => url('media/doc'),
                    'extension' => $request->file('pic')->getClientOriginalExtension(),
                    'size' => $request->file('pic')->getSize(),
                    'title' => $request->file('pic')->getClientOriginalName(),
                    'comment' => $request->input('comment'),
                    'document_status' => $request->input('document_status'),
                    'doc_type' => 2,
                    'base_directory_id' => $root
                ];

                //add in database
                Media::create($pic_object);

                DB::commit();
                return Redirect::back()->with([
                    'info' => 'File Uploaded Successfully',
                ]);

            } catch (\Exception $exception) {

                DB::rollBack();
                Redirect::back()->with([
                    'info' => $exception->getMessage()
                ]);
            }
        }
    }

    public function action(Request $request) {

        $action = $request->input('action');

        try{
            if($request->has('ids')) {
                $ids = $request->input('ids');
                for($i = 0; $i < count($ids); $i++) {
                    $media = Media::where('id', $ids[$i]);

                    switch ($action) {
                        case 'active':
                            $media->update(['status' => 1]);
                            break;
                        case 'inactive':
                            $media->update(['status' => 2]);
                            break;
                    }
                }
            }

            Session::flash('success', "Users ".  ucfirst($action) . " successfully");
            return response(['status' => true, 'message' => 'Media '. ucfirst($action) . ' successfully', 'redirectTo' => url('/doc')]);
        } catch (\Exception $ex) {
            Session::flash('error', "Something went wrong");
            return response(['status' => false, 'message' => 'something went wrong', 'error' => $ex->getMessage()]);
        }

    }

    public function createNewFolder(Request $request)
    {
        $data = $request->input();

        try {

            $name = $data['name'];
            $root = $data['base_id'] == 'root' || $data['base_id'] == 0 ? null : (int) $data['base_id'];
            $user = !empty($root) ? User::find($data['user_id']) : auth()->user();
            if(empty($user->base_directory)){
                $this->createBaseDirectory();
            }

            if($data['base_id'] == "root" || $data['base_id'] == 0){
                $base_path = $user->base_directory;
            }else{

                $folder = Directory::where('user_id', $user->id)->where('id', '=', $root)->first();

                $base_path = !empty($folder) ? $folder->path : $user->base_directory;

            }

            $directory_path = $base_path .'/'. $name;
            $data['path'] = $directory_path;

            $this->createDirectory($data, $directory_path);

            DB::commit();
            $temp = $this->getRootFilesAdnFolder($user, $root, $request);
            $folders = $temp['folders'];
            $files = $temp['files'];

            $root = $data['base_id'];

            return view('doc/all', compact('folders', 'root', 'files', 'user'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['success' => false, 'error' => $exception->getMessage()]);
        }
    }

    public function createBaseDirectory()
    {
        $user = auth()->user();
        $path = 'public/'.$user->id .'-'.date('d-m-Y');
        Storage::makeDirectory($path);
        $user->base_directory = $path;
        $user->save();
        return $path;
    }

    public function createDirectory($data, $path)
    {
        if(Storage::exists($path)){
            throw new \Exception("This directory already exists");
        }

        $user = auth()->user();
        $directory = new Directory;
        $directory->name = $data['name'];
        $directory->user_id = $user->id;
        $directory->base_directory_id = $data['base_id'] == 'root' ? null : $data['base_id'];
        $directory->path = $path;
        $directory->save();

        Storage::makeDirectory($path);
    }

    public function getBaseDirectoryPath()
    {
        $user = auth()->user();
        $path = $user->id .'-'.date('d-m-Y');
        return $path;
    }

    public function isDirectoryExists($path)
    {
        return Storage::exists($path);
    }

    public function getSubFolders(Request $request)
    {
        $user = auth()->user();
        $role = $user->role;

        $root  = $request->input('base_id');
        if($user->is_admin){
            $folders = Directory::where('base_directory_id', '=', $root)
                ->orderBy('name', 'asc')
                ->get();
            $files = Media::where('base_directory_id', '=', $root)
                ->orderBy('name', 'asc')
                ->get();

        }
        else if ($role == 'super_user') {
            $user_ids = array();
            $supervisee = DB::table('supervisors')
                ->select(DB::raw('user_id'))
                ->where('supervisor_id', '=', $user->id)
                ->get()->toArray();
            foreach ($supervisee as $user1) {
                $user_ids[] = $user1->user_id;
            }
            $user_ids[] = $user->id;
            $folders = Directory::where('base_directory_id', '=', $root)->whereIn('user_id', $user_ids)
                ->orderBy('name', 'asc')
                ->get();
            $files = Media::where('base_directory_id', '=', $root)->whereIn('user_id', $user_ids)
                ->orderBy('name', 'asc')
                ->get();
        } else {
            $folders = Directory::where('base_directory_id', '=', $root)->where('user_id', $user->id)
                ->orderBy('name', 'asc')
                ->get();
            $files = Media::where('base_directory_id', '=', $root)->where('user_id', $user->id)
                ->orderBy('name', 'asc')
                ->get();
        }

        return view('doc/all', compact('folders', 'root', 'files', 'user'));
    }

    public function getRootFilesAdnFolder($user, $root, $request)
    {

        $root  = $request->input('base_id');
        $order_by = 'name';
        $order_type = 'desc';
        if ($request->input('sort')) {
            $order_type = $request->input('sort');
        }
        if ($request->input('app_id')) {
            $application = Application::find($request->input('app_id'));
            $mid = $application->mid;
            $dba = $application->dba;
            $files = [];
            $folders = [];
            $folder = Directory::where('name', '=', $mid)->first();
            if (!empty($folder)) {
                $files = Media::where('base_directory_id', $folder->id)
                    ->orderBy($order_by, $order_type)->get();
                $folders = Directory::where('base_directory_id', '=', $folder->id)
                    ->orderBy($order_by, $order_type)->get();
            }


            return ['files' => $files, 'folders' => $folders]; //$folders
        }
        if($user->is_admin){
            $folders = Directory::whereRaw('base_directory_id is null or base_directory_id = 0')
                ->orderBy($order_by, $order_type);
            $files = Media::where('base_directory_id', '=', null)
                ->orderBy($order_by, $order_type);

            if ($request->input('name')) {
                $folders = Directory::where('name', 'like', '%' .$request->input('name'). '%')
                    ->orderBy($order_by, $order_type);
                $files = Media::where('name', 'like', '%' .$request->input('name'). '%')
                    ->orderBy($order_by, $order_type);
            }

        } else {
            $folders = Directory::where('is_users_directory',1)->orWhere('is_merchants_directory', 1)->orderBy($order_by, $order_type);
            $files = Media::where('user_id', $user->id)->orderBy($order_by, $order_type);

            if ($request->input('name')) {
                $folders = Directory::where('name', 'like', '%' .$request->input('name'). '%')
                    ->orderBy($order_by, $order_type);
                $files = Media::where('name', 'like', '%' .$request->input('name'). '%')
                    ->orderBy($order_by, $order_type);
                if ($user->role == 'user') {
                    $folders = $folders->where('user_id', $user->id);
                    $files   = $files->where('user_id', $user->id);
                } else {
                    $user_ids = array();
                    $supervisee = DB::table('supervisors')
                        ->select(DB::raw('user_id'))
                        ->where('supervisor_id', '=', auth()->user()->id)
                        ->get()->toArray();
                    foreach ($supervisee as $user) {
                        $user_ids[] = $user->user_id;
                    }
                    $folders = $folders->whereIn('user_id', $user_ids);
                    $files   = $files->whereIn('user_id', $user_ids);
                }
            }
        }
        $folders = $folders->get();
        $files = $files->get();
        return ['files' => $files, 'folders' => $folders];
    }

    public function deleteFolders(Request $request)
    {
        try{

            $id = $request->input('id');
            $folder = Directory::with('children')->where('id', '=', $id)->first();
            if(!$folder){
                throw new \Exception("Some thing went wrong");
            }

            $folder->children()->delete();
            $folder->delete();
            Storage::deleteDirectory($folder->path);

            // return view('directory', compact('folders', 'root'));

            return response()->json([
                'success' => true,
                'error' => []
            ]);

        }catch(\Exception $e){
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function uploadDocument(Request $request)
    {
        $data = $request->input();
        $root = $data['base_id'] == 'root' || $data['base_id'] == 0 ? null : (int) $data['base_id'];
        $validator = false; //DocManagerValidation::add_edit($request->all(), 'add');

        if ($validator) {
            return response()->json(['error' => 'File is required'], 422);
        } else {
            DB::beginTransaction();
            try{

                $root = $data['base_id'] == 'root' || $data['base_id'] == 0 ? null : (int) $data['base_id'];
                $user = !empty($root) ? User::find($data['user_id']) : auth()->user();
                if(empty($user->base_directory)){
                    $this->createBaseDirectory();
                }

                if(!$root){
                    $base_path = $user->base_directory;
                }else{

                    $folder = Directory::where('user_id', $user->id)->where('id', '=', $root)->first();

                    $base_path = !empty($folder) ? $folder->path : $user->base_directory;

                }

                if (!empty($data['app_id'])) {
                    $application = Application::find($data['app_id']);
                    $mid = $application->mid;
                    $folder = Directory::where('name', '=', $mid)->first();
                    if ($folder) {
                        $root   =  $folder->id;
                        $base_path = $folder->path;
                    }
                }
                $directory_path = 'app/'.$base_path;

                foreach($request->file('pic') as $file_request) {

                    $pic_name = Helper::random_time() . '.' . $file_request->getClientOriginalExtension();
                    $destination_path = storage_path($directory_path);
                    $file_size = $file_request->getSize();
                    $file_orginal_name = $file_request->getClientOriginalName();
                    $file_request->move($destination_path, $pic_name);

                    $url = str_replace('public', '', $base_path);
                    $pic_object = [
                        'real_name' => $file_request->getClientOriginalName(),
                        'name' => $pic_name,
                        'base_url' => $url,
                        'extension' => $file_request->getClientOriginalExtension(),
                        'size' => $file_size,
                        'title' => $file_orginal_name,
                        'comment' => $request->input('comment'),
                        'document_status' => $request->input('document_status'),
                        'doc_type' => 2,
                        'base_directory_id' => $root,
                        'user_id' => $user->id
                    ];

                    //add in database
                    Media::create($pic_object);
                }
                $temp = $this->getRootFilesAdnFolder($user, $root, $request);
                $folders = $temp['folders'];
                $files = $temp['files'];
//                    echo "<pre>";
//                    print_r($folders);
//                    exit;
//                 $directories = Storage::directories($base);
                DB::commit();
                return view('doc/all', compact('folders', 'root', 'files', 'user'));
                return response()->json(['success'=> 'true']);

            }catch (\Exception $exception){
                DB::rollBack();
                return response()->json(['error'=> $exception->getMessage()], 500);
            }
        }
    }

    public function downloadDocument($id)
    {
        $file = Media::find($id);
        if(!$file){
            throw new \Exception("Some thing went wrong");
        }
        $filename = $file->base_url.'/'.$file->name;
        return response()->download(storage_path("app/public/{$filename}"));
    }

    public function deleteFile(Request $request)
    {
        try{

            $id = $request->input('id');
            $file = Media::find($id);

            if(!$file){
                throw new \Exception("Some thing went wrong");
            }

            $filename = $file->base_url.'/'.$file->name;
            Storage::delete(storage_path("app/public/{$filename}"));
            $file->forceDelete();

            return response()->json([
                'success' => true,
                'error' => []
            ]);

        }catch(\Exception $e){
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }

    }
}
