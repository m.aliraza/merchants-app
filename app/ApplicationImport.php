<?php

namespace App\Imports;

use App\Application;
use App\Directory;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Storage;

class ApplicationImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $app =  new Application([

            'mid' => $row[0],
            'acquirer' => $row[1],
            'dba' => $row[2],
            'legal_name' => $row[3],
            'address_1' => $row[4],
            'address_2' => $row[5],
            'city' => $row[6],
            'state' => $row[7],
            'postal_code' => $row[8],
            'country' => $row[9],
            'contact' => $row[10],
            'phone_no' => $row[11],
            'email' => $row[12],
            'user_id' => !empty($row[13]) ? $row[13] : null,
            'open_date' => $row[14],

        ]);
        $this->createApplicationFolder($row[2], !empty($row[13]) ? $row[13] : null);
        return $app;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function createApplicationFolder($dba, $user_id) {
        if (empty($dba)) {
            return;
        }
        $baseAppDirectory = Directory::where('is_merchants_directory', 1)->first();
        if (empty($baseAppDirectory)) {
            $directory = new Directory;
            $directory->name = 'Merchants';
            $directory->base_directory_id =  null;
            $directory->path = 'public/merchants';
            $directory->is_merchants_directory = 1;
            Storage::makeDirectory('public/merchants');
            $directory->save();
            $baseAppDirectory = Directory::where('is_merchants_directory', 1)->first();
        }
        $directory_path = 'public/merchants/'. $dba;
        $directory = new Directory;
        $directory->name = $dba;
        $directory->user_id = $user_id;
        $directory->base_directory_id =  $baseAppDirectory->id;
        $directory->path = $directory_path;
        $directory->save();
        Storage::makeDirectory($directory_path);
    }
}
