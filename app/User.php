<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','display_name', 'company_name', 'phone_no', 'status', 'role', 'media_id','email', 'created_by', 'updated_by','created_at','updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // add media_object relationship
    public function media_object()
    {
        return $this->hasOne(Media::class, 'id','media_id');
    }
    public function medias(){
        return $this->hasMany(Media::class);
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getIsAdminAttribute()
    {
        return $this->role == 'admin' ? true : false;
    }
}
