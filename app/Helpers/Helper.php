<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Models\OvadaCase\OvadaCase;


class Helper {

    // create random key
    public static function random_key(){
        $key = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        $max = strlen($key) - 1;
        for ($i = 0; $i < 36; $i++) {
            $string .= $key[mt_rand(0, $max)];
        }
        return $string;
    }

    // get random time in micro seconds
    public static function random_time(){
        $current_time = round(microtime(true)*1000);
        return $current_time . '_' .rand(1111, 9999);
    }

    // decode base_64 to image
    public static function decode_base_64($path, $base_64_code){

        $image_parts = explode(";base64,", $base_64_code);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_name = Helper::random_time() . '.' . $image_type_aux[1];
        File::put($path . $image_name, base64_decode($image_parts[1]));
        return $image_name;
    }

    public  static function localize_us_number($phone) {
        $numbers_only = preg_replace("/[^\d]/", "", $phone);
        return preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);
    }

    public static function custom_array_join($data=array(),$col_name=null,$glue=', '){

        if($data){
            $temp_array = array();
            foreach ($data as $value) {
                if(array_key_exists($col_name, $value)){
                    $temp_array[] = $value->$col_name;
                }
            }
            if(count($temp_array)) {
                return join($glue, $temp_array);
            }
        }
        return '';
    }
}