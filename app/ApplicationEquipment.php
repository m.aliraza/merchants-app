<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class ApplicationEquipment extends Model
{
    use Notifiable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'application_equipments';

    protected $fillable = [
        'application_id','terminal_id','part_name', 'serial_no', 'item_type', 'item_status', 'created_by', 'updated_by','created_at','updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array  Equipment
     */
    protected $hidden = [
        'created_by', 'updated_by',
    ];
}
