<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationMedia extends Model
{
    protected $fillable = [
        'application_id','media_id','created_by', 'updated_by','created_at','updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array  Equipment
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
