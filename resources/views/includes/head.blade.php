<!DOCTYPE html>
<html lang="en" class="loading">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Apex admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Apex admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Universal Payments</title>
    <meta name="csrf-token" content="<?php echo csrf_token() ?>">
    <link rel="apple-touch-icon" sizes="60x60" href={{url('media/ico/apple-icon-60.png')}}>
    <link rel="apple-touch-icon" sizes="76x76" href={{url('media/ico/apple-icon-76.png')}}>
    <link rel="apple-touch-icon" sizes="120x120" href={{url('media/ico/apple-icon-120.png')}}>
    <link rel="apple-touch-icon" sizes="152x152" href={{url('media/ico/apple-icon-512.png')}}>
    <link rel="shortcut icon" type="image/x-icon" href={{url('/media/ico/favicon.ico')}}>
    <link rel="shortcut icon" type="image/png" href={{url('media/ico/favicon-32.png')}}>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href={{url('/app-assets/fonts/feather/style.min.css')}}>
    <link rel="stylesheet" type="text/css" href={{url('/app-assets/fonts/simple-line-icons/style.css')}}>
    <link rel="stylesheet" type="text/css" href={{url('/app-assets/fonts/font-awesome/css/font-awesome.min.css')}}>
    <link rel="stylesheet" type="text/css" href={{url('/app-assets/vendors/css/perfect-scrollbar.min.css')}}>
    <link rel="stylesheet" type="text/css" href={{url('app-assets/vendors/css/perfect-scrollbar.min.css')}}>
    <link rel="stylesheet" type="text/css" href={{url('/app-assets/vendors/css/perfect-scrollbar.min.css')}}>

    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href={{url('/app-assets/css/app.css')}}>
    <link rel="stylesheet" type="text/css" href={{url('/app-assets/css/custom.css')}}>
    <!-- END APEX CSS-->
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->


    <script src={{url('/app-assets/vendors/js/core/jquery-3.2.1.min.js')}} type="text/javascript"></script>
    <script src={{url('/app-assets/vendors/js/core/popper.min.js')}} type="text/javascript"></script>
    <script src={{url('/app-assets/vendors/js/core/bootstrap.min.js')}} type="text/javascript"></script>
    <script src={{url('/app-assets/vendors/js/perfect-scrollbar.jquery.min.js')}} type="text/javascript"></script>
    <script src={{url('/app-assets/js/notify.js')}} type="text/javascript"></script>
    <script src={{url('/app-assets/vendors/js/prism.min.js')}} type="text/javascript"></script>
{{--    <script src={{url('/app-assets/vendors/js/jquery.matchHeight-min.js')}} type="text/javascript"></script>--}}
{{--    <script src={{url('/app-assets/vendors/js/screenfull.min.js')}} type="text/javascript"></script>--}}
{{--    <script src={{url('/app-assets/vendors/js/pace/pace.min.js')}} type="text/javascript"></script>--}}

    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
{{--    <script src={{url('/app-assets/vendors/js/chartist.min.js')}} type="text/javascript"></script>--}}
{{--    <script src={{url('/app-assets/js/app-sidebar.js')}} type="text/javascript"></script>--}}
{{--    <script src={{url('/app-assets/js/notification-sidebar.js')}} type="text/javascript"></script>--}}
{{--    <script src={{url('/app-assets/js/customizer.js')}} type="text/javascript"></script>--}}
{{--    <script src={{url('/app-assets/js/dashboard1.js')}} type="text/javascript"></script>--}}
    <script src={{url('/app-assets/js/custom-script.js')}} type="text/javascript"></script>
    <script src={{url('/app-assets/js/custom_application-script.js')}} type="text/javascript"></script>
    <script src={{url('/app-assets/js/custom_doc-script.js')}} type="text/javascript"></script>
    <script src={{url('/app-assets/js/application-script.js')}} type="text/javascript"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/12.0.0/classic/ckeditor.js"></script>
    <!-- END PAGE VENDOR JS-->

</head>
