<body data-col="2-columns" class=" 2-columns ">
<div class="wrapper">
<div data-active-color="white" data-background-color="man-of-steel" data-image="media/sidebar-bg/01.jpg" class="app-sidebar">
    <div class="sidebar-header">
        <div class="logo clearfix">
            <a href={{url('home')}} class="logo-text">
                <div class="logo-img">
                    <img src={{url('/media/logo.png')}}>
                </div>
            </a>
        </div>
    </div>

    <div class="sidebar-content">
        <div class="nav-container">
            <ul id="main-menu-navigation" data-menu="menu-navigation" data-scroll-to-active="true" class="navigation navigation-main">
                <li class=" nav-item">
                    <a href={{url('/home')}}>
                        <i class="ft-home"></i>
                        <span data-i18n="" class="menu-title">
                            Dashboard
                        </span>
                    </a>
                </li>
                @if(Auth::user()->role == 'admin')
                    <li class=" nav-item">
                        <a href={{url('/user')}}>
                            <i class="ft-users"></i>
                            <span data-i18n="" class="menu-title">
                               Users Management
                            </span>
                        </a>
                    </li>
                @endif
                <li class=" nav-item">
                    <a href={{url('/app')}}>
                        <i class="ft-book"></i>
                        <span data-i18n="" class="menu-title">
                            Merchant Applications
                        </span>
                    </a>
                </li>
                <li class=" nav-item">
                    <a href={{url('/doc')}}>
                        <i class="ft-file"></i>
                        <span data-i18n="" class="menu-title">
                            Document Repository
                        </span>
                    </a>
                </li>
                <li class=" nav-item">
                    <a href={{url('/import_export')}}>
                        <i class="ft-file"></i>
                        <span data-i18n="" class="menu-title">
                            Import & Export
                        </span>
                    </a>
                </li>


            </ul>
        </div>
    </div>
</div>

    <div class="sidebar-background"></div>
</div>