@extends('layouts.default')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $('.open_date').datepicker({
                format: 'dd-mm-yyyy',
            });
        })
        function addRow() {
            jQuery("#equipment_section").append(jQuery("#forrow").html());
        }
        function removeRow(btn) {
            jQuery(btn).parent().parent().remove();
        }
    </script>

    <style>
    .required label:after {
        color: #e32;
        content: ' *';
        display:inline;
    }
    </style>
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                <div class="row justify-content-md-center">

                    @if(\Illuminate\Support\Facades\Session::has('info'))
                        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> <em
                                    style="color: white"> {!! session('info') !!}</em></div>
                    @endif

                    <div class="row" style="width: 100%">
                        <div class="col-md-12">
                            <div class="card">

                                <div class="card-header">
                                    <h4 class="card-title" id="basic-layout-form">Add Merchant Application</h4>
                                    <hr>
                                </div>

                                <div class="card-content">
                                    <div class="px-3">
                                        {!!Form::open(['url' => route('app.insert'), 'enctype' => 'multipart/form-data', 'id' => 'user_form', 'class' => 'form' ]) !!}
                                        <div class="form-body">

                                            <h4 class="form-section"><i class="ft-file"></i> Merchant Application Details: </h4>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group required">
                                                        <label>MID:</label> <span class="danger"> @if ($errors->has('mid')) {{ $errors->first('mid') }} @endif </span>
                                                        {!! Form::text('mid', null, array('class'=>'form-control input-lg',)) !!}
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group required">
                                                        <label>Acquirer: <span
                                                                    class="danger"> @if ($errors->has('acquirer')) {{ $errors->first('acquirer') }} @endif </span>
                                                        </label>
                                                        {!! Form::text('acquirer', null, array('class'=>'form-control input-lg',)) !!}
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group required">
                                                        <label>DBA: </label> <span class="danger"> @if ($errors->has('dba')) {{ $errors->first('dba') }} @endif </span>
                                                        {!! Form::text('dba', null, array('class'=>'form-control input-lg',)) !!}

                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Legal Name: <span
                                                                    class="danger"> @if ($errors->has('legal_name')) {{ $errors->first('legal_name') }} @endif </span></label>
                                                        {!! Form::text('legal_name', null, array('class'=>'form-control input-lg',)) !!}
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group required">
                                                        <label>Address 1: <span
                                                                    class="danger"> @if ($errors->has('address_1')) {{ $errors->first('address_1') }} @endif </span></label>
                                                        {!! Form::text('address_1', null, array('class'=>'form-control input-lg',)) !!}
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address 2:</label>
                                                        {!! Form::text('address_2', null, array('class'=>'form-control input-lg',)) !!}
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>City: <span
                                                                    class="danger"> @if ($errors->has('city')) {{ $errors->first('city') }} @endif </span></label>
                                                        {!! Form::text('city', null, array('class'=>'form-control input-lg',)) !!}
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Province: <span
                                                                    class="danger"> @if ($errors->has('state')) {{ $errors->first('state') }} @endif </span></label>
                                                        {!! Form::text('state', null, array('class'=>'form-control input-lg',)) !!}
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group required">
                                                        <label>Postal Code/ZIP: <span
                                                                    class="danger"> @if ($errors->has('postal_code')) {{ $errors->first('postal_code') }} @endif </span></label>
                                                        {!! Form::text('postal_code', null, array('class'=>'form-control input-lg',)) !!}

                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group required">
                                                        <label>Country: <span
                                                                    class="danger"> @if ($errors->has('country')) {{ $errors->first('country') }} @endif </span></label>
                                                        {!! Form::text('country', null, array('class'=>'form-control input-lg',)) !!}
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group required">
                                                        <label>Contact: <span
                                                                    class="danger"> @if ($errors->has('contact')) {{ $errors->first('contact') }} @endif </span></label>
                                                        {!! Form::text('contact', null, array('class'=>'form-control input-lg',)) !!}

                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Phone No: <span
                                                                    class="danger"> @if ($errors->has('phone_no')) {{ $errors->first('phone_no') }} @endif </span></label>
                                                        {!! Form::text('phone_no', null, array('class'=>'form-control input-lg',)) !!}
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group required">
                                                        <label>E-mail: <span
                                                                    class="danger"> @if ($errors->has('email')) {{ $errors->first('email') }} @endif </span></label>
                                                        {!! Form::email('email', null, array('class'=>'form-control input-lg',)) !!}

                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>User/Agent: <span class="danger"> @if ($errors->has('user_id')) {{ $errors->first('user_id') }} @endif </span></label>
                                                        <select class="form-control required" name="user_id">
                                                            <option value="" selected> select</option>
                                                            @foreach($users as $user)
                                                                <option value={{$user->id}}{{ old('user_id') == $user->id ? 'selected' : ''}} > {{$user->first_name}} &nbsp; {{ $user->last_name }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Date Open: <span
                                                                    class="danger"> @if ($errors->has('open_date')) {{ $errors->first('open_date') }} @endif </span></label>
                                                        {!! Form::text('open_date', null, array('class'=>'form-control input-lg open_date', 'readonly' => true)) !!}
                                                    </div>
                                                </div>

											</div>
                                            <h4 class="form-section"><i class="ft-file-text"></i> Equipment Information: </h4>

                                            <section id="equipment_section">

                                                <div class="row" >

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>Terminal ID: <span class="danger"> @if ($errors->has('terminal_id')) {{ $errors->first('terminal_id') }} @endif </span></label>
                                                            {!! Form::text('equipment[][terminal_id]', null, array('class'=>'form-control input-lg')) !!}
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Part Name: <span class="danger"> @if ($errors->has('part_name')) {{ $errors->first('part_name') }} @endif </span></label>
                                                            {!! Form::text('equipment[][part_name]', null, array('class'=>'form-control input-lg')) !!}

                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Serial Number: <span class="danger"> @if ($errors->has('serial_no')) {{ $errors->first('serial_no') }} @endif </span></label>
                                                            {!! Form::text('equipment[][serial_no]', null, array('class'=>'form-control input-lg')) !!}
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label>Type: <span class="danger"> @if ($errors->has('item_type')) {{ $errors->first('item_type') }} @endif </span></label>
                                                            <select class="form-control" name="equipment[][item_type]">
                                                                <option value="" selected> select</option>
                                                                <option value=rental {{ old('item_type') == 'rental' ? 'selected' : ''}}>
                                                                    Rental
                                                                </option>
                                                                <option value=purchased {{ old('item_type') == 'purchased' ? 'selected': ''}}>
                                                                    Purchased
                                                                </option>
                                                                <option value=leased {{ old('item_type') == 'leased' ? 'selected' : ''}}>
                                                                    Leased
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label>Status: <span class="danger"> @if ($errors->has('item_status')) {{ $errors->first('item_status') }} @endif </span></label>
                                                            <select class="form-control" name="equipment[][item_status]">
                                                                <option value="" selected> select</option>
                                                                <option value=active {{ old('item_status') == 'active' ? 'selected' : ''}}>
                                                                    Active
                                                                </option>
                                                                <option value=inactive {{ old('item_status') == 'inactive' ? 'selected': ''}}>
                                                                    Inactive
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <button type="button" class="btn btn-primary" style="margin-top: 30px"  onclick="addRow()" id="add_new_equipment_row">
                                                            Add more equipment
                                                        </button>
                                                    </div>

                                                </div>


                                            </section>


                                            <div class="form-actions">
                                                {{--                                                    <button type="submit" class="btn btn-raised btn-raised btn-primary">--}}
                                                <input type="submit" name="name" class="btn btn-success" value="Submit Merchant Application">
                                                {{--                                                    </button>--}}
                                            </div>

                                            {{ Form::close() }}
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div style="display:none;" id="forrow">
        <div class='row'>
            <div class='col-md-2'>
                <div class='form-group'>
                    <label>Terminal ID: <span class='danger'></span></label>
                    <input type='text' name='equipment[][terminal_id]' class='form-control input-lg' />
                </div>
            </div>
            <div class='col-md-3'>
                <div class='form-group'>
                    <label>Part Name: <span class='danger'></span></label>
                    <input type='text' name='equipment[][part_name]' class='form-control input-lg' required />
                </div>
            </div>
            <div class='col-md-3'>
                <div class='form-group'>
                    <label>Serial Number: <span class='danger'></span></label>
                    <input type='text' name='equipment[][serial_no]' class='form-control input-lg' required />
                </div>
            </div>
            <div class='col-md-1'>
                <div class='form-group'>
                    <label>Type: <span class='danger'></span></label>
                    <select class='form-control' name='equipment[][item_type]' required>
                        <option value='' selected> select</option>
                        <option value='rental'>Rental</option>
                        <option value='purchased'>Purchased</option>
                        <option value='leased'>Leased</option>
                    </select>
                </div>
            </div>
            <div class='col-md-1'>
                <div class='form-group'>
                    <label>Status: <span class='danger'></span></label>
                    <select class='form-control' name='equipment[][item_status]' required>
                        <option value='' selected> select</option>
                        <option value='active'>Active</option>
                        <option value='inactive'>Inactive</option>
                    </select>
                </div>
            </div>
            <div class='col-md-2'><button type='button' class='btn btn-danger rem' style='margin-top: 30px' onclick="removeRow(this)"   >Remove</button></div></div></div>
    </div>
@endsection
