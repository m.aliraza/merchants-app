<style>
    .required label:after {
        color: #e32;
        content: ' *';
        display:inline;
    }
</style>
<div class="px-3">

    {!!Form::open(['url' => route('doc.insert'), 'enctype' => 'multipart/form-data', 'id' => 'document_upload', 'class' => 'form form-horizontal form-bordered' ]) !!}

        <div class="form-body">
            <div class="form-group row">
                <label class="col-md-3 label-control" for="eventRegInput1"> File Title: </label>
                <div class="col-md-9">
                    {!! Form::text('title', null, array('class'=>'form-control required input-lg',)) !!}
                    <span class="danger"> @if ($errors->has('title')) {{ $errors->first('title') }} @endif </span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 label-control" for="eventRegInput1"> File Status: </label>
                <div class="col-md-9">
                    {!! Form::text('document_status', null, array('class'=>'form-control input-lg',)) !!}
                    <span class="danger"> @if ($errors->has('title')) {{ $errors->first('title') }} @endif </span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 label-control" for="eventRegInput1"> Description: </label>
                <div class="col-md-9">
                    {!! Form::textarea('comment', null, array('class'=>'form-control input-lg',)) !!}
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 label-control" for="eventRegInput1"> Upload Document: </label>
                <div class="col-md-9">
                    {!! Form::file('pic', null, array('class'=>'form-control input-lg',)) !!}
                    <span class="danger"> @if ($errors->has('pic')) {{ $errors->first('pic') }} @endif </span>
                </div>
            </div>

            <div class="form-group row">
                <div>
                    <div class="col-md-12 form-group">
                        <button type="submit" class="btn btn-info btn-success submit_btn upload_file">Save</button>
                    </div>
                </div>
            </div>
           <input id="fileUserId" name="user_id" type="hidden" >
        </div>
    {{ Form::close() }}
</div>
