<?php
	$all = $data->toArray();
	$applications =  (object) $all['data'];
	$total   = $all['total'];
	$current_page = $all['current_page'];
	$sort = empty(Request::input('sort')) ? '' : Request::input('sort');
	$user_id = empty(Request::input('user_id')) ? '' : Request::input('user_id');
	$status = empty(Request::input('status')) ? '' : Request::input('status');
	$search_keyword = empty(Request::input('search_keyword')) ? '' : Request::input('search_keyword');
// ?>
@extends('layouts.default')
@section('css')
    <style>
        .card-body {
            overflow-x: auto;
        }
        .action-wrapper {
            display: flex;
            align-items: center;
            margin-top: 10px;
        }
        .action-wrapper a {
            margin-right: 5px;
        }
    </style>
@endsection
@section('content')
   <script src={{url('/app-assets/js/jquery.simplePagination.js')}} type="text/javascript"></script>
<link href={{url('/app-assets/css/simplePagination.css')}} rel="stylesheet" type="text/css" />
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">

                <section id="striped-light">
                    <div class="row">
                        <div class="col-sm-12">
                            @if(Auth::user()->role == 'admin')
                                <button  type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
                                    <a href={{route('app.add')}} style="color: white">Add New Application</a>
                                </button>
                            @endif
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Merchant Applications Management (Listings)	</h4>
                                    <hr>
                                    {!!Form::open(['url' => route('app'), 'class' => 'form form-horizontal form-bordered' ,'method'=> 'GET']) !!}

                                    <div class="form-body row">
                                        <div class="col-md-2">
                                            {!! Form::text('search_keyword', !empty(Request::input('search_keyword')) ? Request::input('search_keyword') : '', array('class'=>'form-control input-lg','placeholder'=>'Search')) !!}
                                        </div>
                    @if(Auth::user()->role == 'admin')
                        <div class="col-md-2">
                            <select class="form-control required" name="user_id">
                                <option value="" selected> Select user</option>
                                    @foreach($users as $user)
                                     <option value={{$user->id}} {{ Request::input('user_id')  ==  $user->id ? 'selected' : '' }}> {{$user->first_name}} &nbsp; {{ $user->last_name }} </option>
                                    @endforeach
                            </select>
                        </div>
                        @endif
                        @if(Auth::user()->role == 'super_user')
                                            <div class="col-md-2">
                                                <select class="form-control required" name="user_id">
                                                    <option value="" selected> Select user</option>
                                                    @foreach($qry as $user)
                                                        <option value={{$user->id}} {{ Request::input('user_id')  ==  $user->id ? 'selected' : '' }}> {{$user->first_name}} &nbsp; {{ $user->last_name }} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                    @endif

                                        <div class="col-md-2">
                                            <select class="form-control" name="status">
                                                <option value="" {{ empty(Request::input('selected')) ? 'selected' : '' }}> All Status</option>
                                                <option value= 2 {{ Request::input('selected') == 2 ? 'selected' : '' }}> Open </option>
                                                <option value= 3 {{ Request::input('selected') == 3 ? 'selected' : '' }}> Closed</option>
                                                <option value= 4 {{ Request::input('selected') == 4 ? 'selected' : '' }}> Pending</option>
                                            </select>
                                        </div>
					   <div class="col-md-2">
                                                <select class="form-control" name="sort">
                                                    <option value="" {{ empty(Request::input('sort')) ? 'selected' : '' }} >Sort Order: </option>
                                                    <option value="asc" {{ Request::input('sort') == 'asc' ? 'selected' : '' }}>A-Z</option>
                                                    <option value="desc" {{ Request::input('sort') == 'desc' ? 'selected' : '' }}>Z-A</option>
                                                </select>
                                            </div>
                                        <div class="col-md-1">
                                            <button type="submit" class="btn btn-info btn-success submit_btn">Filter</button>
                                        </div>
                                        {{ Form::close() }}
                                        @if(Auth::user()->role == 'super_user')
                                            {!!Form::open(['url' => route('app.export'), 'enctype' => 'multipart/form-data', 'id' => 'user_form', 'class' => 'form' ]) !!}
                                            <input type="submit" name="name" class="btn btn-success" value="Export Data">
                                            {{ Form::close() }}
                                        @endif
                                         <div class="col-md-1">
                                             <select class="form-control" name="action" id="action">
                                                <option value="" selected> All </option>
                                                <option value="pending"> Pending</option>
                                                 <option value="active"> Open </option>
                                                 <option value="closed"> Closed</option>
                                                 <option value="delete"> Delete </option>
                                             </select>
                                         </div>
                                        <div class="row">
                                         <div class="col-md-2">
                                             <button type="button" class="btn btn-info btn-warning submit_btn" style="color: white;" id="apply"  data-url="{{ url('/app/action') }}">Apply</button>

                                         </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <table class="table table-striped" id="mytable">
                                            <thead>
                                            <tr>
                                                <th>
                                                    <div class="custom-control custom-checkbox m-0">
                                                        <input type="checkbox" class="custom-control-input" id="item-select-all">
                                                        <label class="custom-control-label" for="item-select-all"></label>
                                                    </div>
                                                </th>
                                                <th>MID</th>
                                                <th>DBA</th>
                                                <th>Address</th>
                                                <th>City</th>
                                                <th>Province</th>
                                                <th>Country</th>
                                                <th>Contact</th>
                                                <th>Phone No</th>
                                                <th>Date Open</th>
                                                <th>Agent</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($applications as $application)
                                                <tr>
                                                    <td> <div class="custom-control custom-checkbox m-0">
                                                            <input type="checkbox" class="custom-control-input checkbox" id="item{{$application['id']}}" value="{{$application['id']}}">
                                                            <label class="custom-control-label" for="item{{$application['id']}}"></label>
                                                        </div>
                                                    </td>
                                                    <td> {{$application['mid']}}</td>
                                                    <td> {{$application['dba']}}</td>
                                                    <td>{{$application['address_1']}}</td>
                                                    <td>{{$application['city']}}</td>
                                                    <td>{{$application['state']}}</td>
                                                    <td>{{$application['country']}}</td>
                                                    <td>{{$application['contact']}}</td>
                                                    <td>{{$application['phone_no']}}</td>
                                                    <td>{{$application['open_date']}}</td>
                                                    <td>

                                                        @if(isset($application['user']['first_name']))
                                                            {{$application['user']['first_name']}} {{$application['user']['last_name']}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($application['status'] == 2)
                                                            <a class="btn white btn-round btn-success">Open</a>
							                            @elseif($application['status'] == 1)
                                                            <a class="btn white btn-round btn-info">Pending</a>
                                                        @elseif($application['status'] == 3)
                                                            <a class="btn white btn-round btn-danger">Closed</a>
                                                        @elseif($application['status'] == 4)
                                                            <a class="btn white btn-round btn-danger">Pending</a>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <div class="action-wrapper">
                                                            @if(Auth::user()->role == 'admin')
                                                                <a href={{route('app.edit',$application['id'])}} class="warning" data-original-title="" title="">
                                                                    <i class="ft-edit-2" style="font-size: 22px"></i>
                                                                </a>
                                                            @endif
                                                            <a href={{route('app').'/'.$application['id']}} class="primary" data-original-title="" title="">
                                                                <i class="ft-eye" style="font-size: 22px"></i>
                                                            </a>
                                                            <a href="{{route('app').'/'.$application['id']}}#comment-box" class="primary" data-original-title="" title="">
                                                                <i class="fa fa-comment-o" aria-hidden="true" style="font-size: 22px"></i>
                                                            </a>
                                                            <a href="{{route('doc')}}?app_id={{$application['id']}}" class="primary" data-original-title="" title="">
                                                                <i class="fa fa-file" aria-hidden="true" style="font-size: 22px"></i>
                                                            </a>
                                                        </div>
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
					<div id="pagination"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<script>
$(document).ready(function () {
	$("#pagination").pagination({
        items: {{$total}},
        itemsOnPage: 25,
        cssStyle: 'light-theme',
	currentPage: {{$current_page}},
	onPageClick: function (page, event) {
	    var url = window.location.protocol + "//" + window.location.host + window.location.pathname + '?page=' + page
        var sort = '{{ $sort }}';
        var user_id = '{{ $user_id  }}';
        var status = '{{ $status  }}';
        var search_keyword = '{{ $search_keyword  }}';
        if (sort !== '') {
            url += '&sort='+ sort
        }
        if (user_id !== '') {
            url += '&user_id='+ user_id
        }
        if (status !== '') {
            url += '&status='+ status
        }
        if (search_keyword !== '') {
            url += '&search_keyword='+ search_keyword
        }
        window.location = url
	}
    });
});
</script>
@endsection


