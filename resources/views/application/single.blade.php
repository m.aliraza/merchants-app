@extends('layouts.default')
@section('css')
<style>
    .table th, .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
    text-align: left;
}
</style>
@endsection
@section('content')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="text-bold-600 warning">Application Detail Page
                                            @if($application->status == 2)
                                                <a class="btn white btn-round btn-success">Open</a>
                                            @elseif($application->status == 3)
                                                <a class="btn white btn-round btn-danger">Closed</a>
                                            @endif
                                    </h5>
                                </div>
                                <hr>

                                <div class="row">

                                    <div class="col-12 col-md-6 col-lg-4">
                                        <ul class="no-list-style">
                                            <li class="mb-2">
                                                <span class="text-bold-500 primary"> MID: </span>
                                                <span class="d-block overflow-hidden">{{$application->mid}}</span>
                                            </li>
                                            <hr>
											<li class="mb-2">
												<span class="text-bold-500 primary"> Legal Name: </span>
												<span class="d-block overflow-hidden">{{$application->legal_name}}</span>
											</li>
											<hr>
											<li class="mb-2">
												<span class="text-bold-500 primary">City:</span>
												<span class="d-block overflow-hidden">{{$application->city}}</span>
											</li>
											<hr>
											<li class="mb-2">
                                                <span class="text-bold-500 primary">Country:</span>
                                                <span class="d-block overflow-hidden">{{$application->country}}</span>
                                            </li>
                                            <hr>
											<li class="mb-2">
                                                <span class="text-bold-500 primary">E-mail:</span>
                                                <span class="d-block overflow-hidden">{{$application->email}}</span>
                                            </li>
                                            <hr>
										</ul>
                                    </div>

                                    <div class="col-12 col-md-6 col-lg-4">
                                        <ul class="no-list-style">
                                           <li class="mb-2">
                                                <span class="text-bold-500 primary"> Acquirer: </span>
                                                <span class="d-block overflow-hidden">{{$application->acquirer}}</span>
                                            </li>
                                            <hr> 
											<li class="mb-2">
                                                <span class="text-bold-500 primary">Address 1:</span>
                                                <span class="d-block overflow-hidden">{{$application->address_1}}</span>
                                            </li>
                                            <hr>
											<li class="mb-2">
												<span class="text-bold-500 primary">Province:</span>
												<span class="d-block overflow-hidden">{{$application->state}}</span>
											</li>
											<hr>
											<li class="mb-2">
                                                <span class="text-bold-500 primary">Contact:</span>
                                                <span class="d-block overflow-hidden">{{$application->contact}}</span>
                                            </li>
                                            <hr>
											<li class="mb-2">
                                                <span class="text-bold-500 primary">Agent:</span>
                                                <span class="d-block overflow-hidden">
                                                    @if(isset($application->user->display_name))
                                                        {{$application->user->display_name}}
                                                    @endif
                                                </span>
                                            </li>
                                            <hr>
                                        </ul>
                                    </div>

                                    <div class="col-12 col-md-6 col-lg-4">
                                        <ul class="no-list-style">
											<li class="mb-2">
												<span class="text-bold-500 primary"> DBA: </span>
												<span class="d-block overflow-hidden">{{$application->dba}}</span>
											</li>
											<hr>
											<li class="mb-2">
                                                <span class="text-bold-500 primary">Address 2:</span>
                                                <span class="d-block overflow-hidden">{{$application->address_2}}</span>
                                            </li>
                                            <hr>
											<li class="mb-2">
                                                <span class="text-bold-500 primary">Postal Code:</span>
                                                <span class="d-block overflow-hidden">{{$application->postal_code}}</span>
                                            </li>
                                            <hr>
											<li class="mb-2">
                                                <span class="text-bold-500 primary">Phone No:</span>
                                                <span class="d-block overflow-hidden">{{$application->phone_no}}</span>
                                            </li>
                                            <hr>
											<li class="mb-2">
                                                <span class="text-bold-500 primary">Date Open:</span>
                                                <span class="d-block overflow-hidden">{{$application->open_date}}</span>
                                            </li>
                                            <hr>
                                        </ul>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="text-bold-600 warning">Equipment Information</h5>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Terminal ID</th>
                                            <th>Part Name</th>
                                            <th>Serial No</th>
                                            <th>Item Type</th>
                                            <th>Item Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($application->equipments as $equipment)
                                            <tr>
                                                <td> {{$equipment->terminal_id}}</td>
                                                <td> {{$equipment->part_name}}</td>
                                                <td>{{$equipment->serial_no}}</td>
                                                <td>{{$equipment->item_type}}</td>
                                                <td>
                                                    @if($equipment->item_status == 'active')
                                                        <a class="btn white btn-round btn-success">Active</a>
                                                    @elseif($equipment->item_status == 'inactive')
                                                        <a class="btn white btn-round btn-danger">Inactive</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="text-bold-600 warning">Application Comments</h5>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width: 10%">User Name</th>
                                            <th style="width: 10%">Date & Time</th>
                                            <th style="width: 80%">Comment</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($application->comments as
                                        $comment)
                                        @php
                                            $cuser = App\User::find($comment->user_id);
                                        @endphp
                                            <tr>
                                                <td>
                                                    {{$cuser->first_name .' '. $cuser->last_name}}
                                                </td>
                                                <td>{{$comment->created_at}}</td>
                                                <td>{!! html_entity_decode($comment->comment) !!}</td>
                                        @endforeach
                                            </tr>
                                        </tbody>
                                    </table>

                                    {!!Form::open(['url' => route('app.add_comment'), 'enctype' => 'multipart/form-data', 'id' => 'user_form', 'class' => 'form form-horizontal', 'novalidate' ]) !!}
                                    <div id="comment-box" class="form-body">
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                {!! Form::hidden('application_id', $application->id, array('class'=>'form-control input-lg',)) !!}
                                                {!! Form::textarea('comment', null, array( 'id' => "comment-editor", 'class'=>'form-control input-lg', 'required', 'placeholder' => 'add comment here')) !!}
                                                <span class="danger"> @if ($errors->has('comment')) {{ $errors->first('comment') }} @endif </span>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" id="submit-comment" class="btn btn-info btn-success submit_btn">Submit Comment</button>
                                            </div>
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

        let editor;
        ClassicEditor
        .create( document.querySelector( '#comment-editor' ), {
            toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
        } ).then( newEditor => {
            editor = newEditor;
        } )
        .catch( error => {
            console.log( error );
        } );

        // $(document).on('click','#submit-commen',function(e){

        //     e.preventDefault();
        //     e.stopPropagation();
        //     const editorData = editor.getData();

        //     var request = $.ajax({
        //         url: "script.php",
        //         method: "POST",
        //         data: { id : menuId },
        //         dataType: "html"
        //     });

        //     request.done(function( msg ) {
        //         $( "#log" ).html( msg );
        //     });

        //     request.fail(function( jqXHR, textStatus ) {
        //         alert( "Request failed: " + textStatus );
        //     });
        // });
    </script>
@endsection
