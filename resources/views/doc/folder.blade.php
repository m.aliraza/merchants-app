﻿@extends('layouts.default')
@section('css')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<style>
    .folder-wrapper > img {
        width: 110px;
    }
    .edit-icon  {
        cursor: pointer;
        display: flex;
    }
    .edit-icon .fa-pencil {
        display: none;
    }


    .folder-block {
        position: relative;
    }
    @if(auth()->user()->role != 'user')
        .folder-block:hover .delete-icon {
            display: block;
        }
    @endif
    .delete-icon {
        display: none;
        position: absolute;
        top: 10px;
        left: 85px;
        cursor: pointer;
        color: red;
    }
    .download-icon {
        display: none;
        position: absolute;
        top: 10px;
        left: 85px;
        cursor: pointer;
        color: black;
    }
    .folder-block:hover .download-icon {
        display: block;
    }

</style>
@endsection
@section('content')

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">

                <section id="striped-light">
                    <div class="row">
                        <div class="col-sm-12">
                            {{-- <nav aria-label="breadcrumb" class="pt-2">
                              <ol class="breadcrumb" style="background-color:#f5f7fa;">
                               <li class="breadcrumb-item"><a href="#">Home</a></li>
                               <li class="breadcrumb-item"><a href="#">Library</a></li>
                             </ol>
                           </nav> --}}
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">All Documents</h4>
                                    <hr>
                                    <form method="GET" action="" accept-charset="UTF-8" class="form form-horizontal form-bordered">
                                        <div class="form-body row">
                                            <div class="col-md-4">
                                                <input class="form-control input-lg" value="{{ empty(Request::input('name')) ? '' : Request::input('name')  }}" placeholder="Search" name="name" type="text">
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control" name="sort">
                                                    <option value="" {{ empty(Request::input('sort')) ? 'selected' : '' }} >Sort Order: </option>
                                                    <option value="desc" {{ Request::input('sort') == 'desc' ? 'selected' : '' }}>A-Z</option>
                                                    <option value="asc" {{ Request::input('sort') == 'asc' ? 'selected' : '' }}>Z-A</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-info btn-success submit_btn">Filter Results</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-content">
                                    <div id="card-body" class="card-body">
                                        <div>
                                            @include('doc/all')
                                        </div>
                                        <div id="file-section" style="display:none">
                                            @include('doc.file')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        $(document).on('click', '#new-folder', function(){
            $('#file-section').hide();
            $('#repo').show();
            let user_id = $(this).data('uid');
            createFolder(user_id);
        });

        function createFolder(user_id){
            Swal.fire({
                title: 'Create new folder',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Create',
                showLoaderOnConfirm: true,
                preConfirm: (name) => {
                    let url = `{{route('create.folder')}}`;
                    let data = {};
                    data.name = name;
                    data.base_id = $("#parentId").val();
                    data.user_id = user_id;
                    return axios.post(url, data)
                        .then(function (response) {
                            $("#repo").empty().append(response.data);
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                console.log(result);
                if (result.success) {
                    Swal.fire({
                        title: `Folder Created`
                    })
                }
            })
        }

        $(document).on('click', '.edit-name', function(e){
            e.preventDefault();
            e.stopPropagation();

        });

        $(document).on('click', '.open-folder', function(e){
            e.preventDefault();
            e.stopPropagation();

            let id = $(this).data('id');
            let url = `{{route('get.subfolders')}}`;
            let data = {};
            data.base_id = id;
            data.user_id = $(this).closest('.folder').find('.folder_user_id').val();
            axios.post(url, data)
                .then(function (response) {

                    $("#repo").empty().append(response.data);
                })
                .catch(function (error) {
                    console.log(error);
                });
        });

        function deleteFolder(data){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                let url = `{{route('delete.folder')}}`;
                if (result.value) {
                    axios.post(url, data)
                        .then(function (response) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );

                            location.reload(true);
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            })
        }

        $(document).on('click', '.delete-icon', function(e){

            e.preventDefault();
            e.stopPropagation();
            let folder = $(this).closest('.folder').find('.folder-id').val();
            let data = {};
            data.id = folder;
            data.user_id = $(this).closest('.folder').find('.folder_user_id').val();
            deleteFolder(data);
        });

        $(document).on('click', '#open-file-section', function(e){

            let user_id = $(this).data('uid');
            $("#fileUserId").val(user_id);
            $('#file-section').show(600);
            $('#repo-hide').hide(600);

        });

        $(document).on('submit', '#document_upload', function(e){
            e.preventDefault();
            let parent = $("#parentId").val();
            let formData = new FormData(this);
            formData.append('base_id', parent);
            formData.append('app_id', '{{ app('request')->input('app_id') }}');
            let url = `{{route('document.upload')}}`;
            $.ajax({
                beforeSend:function(){
                    $('#success').empty();
                    $('.progress-bar').text('0%');
                    $('.progress-bar').css('width','0')
                },
                uploadProgress:function(event, position, total, percentComplete) {
                    $('.progress-bar').text(percentComplete + '0%');
                    $('.progress-bar').css('width', percentComplete + '0%');
                },
                success:function(data){
                        // $('#success').html('<div class="text-success text-center"><b>' + data.success + '</b></div><br /><br />')
                        $('#success').append();
                        $('.progress-bar').text('Uploaded');
                        $('.progress-bar').css('width', '100%');
                },
            });
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                dataType: 'html',
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){

                },
                success: function(response){
                    $.notify("File Uploaded Successfully.","success");
                    var x = $(response).find('#files-area');
                    // $("#repo").hide(600);

                    // $("#repo").hide();
                    // $(".all-folders-row").style.display = "none";
                    // $(".all-folders-row").hide();
                    // $(".open-folder .folder-wrapper").style.display= "none";
                    $(".card-content").append(x);
                    // $(".files-area").show();
                    // $(".folder-wrapper").hide(600);

                    // $('#folder-area').hide(5000);
                },
                error: function(response) {
                    var res = JSON.parse(response.responseText);
                    console.log(res);
                    $.notify(res.error);
                }

            });

            setTimeout(function () {
                window.location.reload(1);
            }, 2000);
        });


        $(document).on('click', '.delete-file', function(e){

            e.preventDefault();
            e.stopPropagation();
            let file = $(this).closest('.folder').find('.file-id').val();
            let data = {};
            data.id = file;
            data.user_id = $(this).closest('.folder').find('.file_user_id').val();
            deleteFile(data);
        });

        function deleteFile(data){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                let url = `{{route('delete.file')}}`;
                if (result.value) {
                    alert(result);
                    axios.post(url, data)
                        .then(function (response) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );

                            location.reload(true);
                            // window.location.reload(false);
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                }
            })
        }

    </script>

@endsection
