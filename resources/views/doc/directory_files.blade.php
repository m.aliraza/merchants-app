
<div class="all-folders-row d-flex">
    @foreach($files as $file)
        <div class="folder-block mr-5 folder">
            @if($file->extension == 'pdf')
                {{-- <i class="fa fa-download download-icon" aria-hidden="true"></i> --}}
                {{-- <i class="fa fa-minus-square-o delete-icon" aria-hidden="true"></i> --}}
                <div class="folder-wrapper" data-id="{{$file->id}}" >
                    <a href="{{route('document.download', [$file->id])}}">
                        <img width="60px" src="{{asset('images/pdf.png')}}">
                    </a>
                    <div class="menu">
                        {{$file->title}} <i class="fa fa-minus-square-o delete-file" aria-hidden="true"></i>
                    </div>
                </div>
            @elseif($file->extension == 'doc' || $file->extension == 'xls' || $file->extension == 'xlsx' || $file->extension == 'docx')
                <div class="folder-wrapper" data-id="{{$file->id}}" >
                    <a href="{{route('document.download', [$file->id])}}">
                        <img width="60px" src="{{asset('images/file.png')}}">
                    </a>
                    <div class="menu">
                        {{$file->title}} <i class="fa fa-minus-square-o delete-file" aria-hidden="true"></i>
                    </div>
                </div>
            @else
                <div class="folder-wrapper" data-id="{{$file->id}}" >
                    <a href="{{route('document.download', [$file->id])}}">
                        <img width="80px" src="{{asset('storage/'.$file->base_url.'/'.$file->name)}}">
                    </a>
                    <div class="menu">
                        {{$file->title}} <i class="fa fa-minus-square-o delete-file" style="color:red" aria-hidden="true"></i>
                    </div>
                </div>
            @endif
            <input class="file-id" type="hidden" value="{{$file->id}}">
            <input class="file_user_id" type="hidden" value="{{$file->user_id}}">
        </div>
    @endforeach

</div>
