@extends('layouts.default')
@section('content')

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">

                <div class="row justify-content-md-center">

                    @if(\Illuminate\Support\Facades\Session::has('info'))
                        <div class="alert alert-success"> <span class="glyphicon glyphicon-ok"></span> <em style="color: white"> {!! session('info') !!}</em> </div>
                    @endif

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="bordered-layout-card-center">Add File</h4>
                            </div>
                            <div class="card-body">
                                <div class="px-3">

                                    {!!Form::open(['url' => route('doc.insert'), 'enctype' => 'multipart/form-data', 'id' => 'user_form', 'class' => 'form form-horizontal form-bordered' ]) !!}

                                    <div class="form-body">
                                        <input type="hidden" name="app_id" val="{{ app('request')->input('app_id') }}"/>
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="eventRegInput1"> File Title: </label>
                                            <div class="col-md-9">
                                                {!! Form::text('title', null, array('class'=>'form-control input-lg',)) !!}
                                                <span class="danger"> @if ($errors->has('title')) {{ $errors->first('title') }} @endif </span>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="eventRegInput1"> Description: </label>
                                            <div class="col-md-9">
                                                {!! Form::textarea('comment', null, array('class'=>'form-control input-lg',)) !!}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="eventRegInput1"> Upload Document: </label>
                                            <div class="col-md-9">
                                                {!! Form::file('pic', null, array('class'=>'form-control input-lg',)) !!}
                                                <span class="danger"> @if ($errors->has('pic')) {{ $errors->first('pic') }} @endif </span>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div>
                                                <div class="col-md-12 form-group">
                                                    <button type="submit" class="btn btn-info btn-success submit_btn">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
