<div id="repo">
    @if(Auth::user()->is_admin)
        <button type="button" id="open-file-section" data-uid="{{$user->id}}" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
            <a href="javascript:void(0)" style="color: white">Add New Document</a>
        </button>
        <button type="button" id="new-folder" data-uid="{{$user->id}}" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
            <a href="javascript:void(0)" style="color: white">New Folder</a>
        </button>
    @endif
    <div id="repo-hide">
        <div id="folder-area">
            @include('directory')
        </div>
        <div id="files-area">
            <div class="row d-flex m-0">
                @include('doc/directory_files')
            </div>
            {{--            @include('doc/directory_files')--}}
        </div>
    </div>
</div>

