<style>
    .required label:after {
        color: #e32;
        content: ' *';
        display:inline;
    }
    /*.progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; }*/
    /*.bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }*/
    /*.percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}*/
    /*#loadingMessage {*/
    /*    position: absolute;*/
    /*    top: 2px;*/
    /*    left: 2px;*/
    /*    z-Index: 1050;*/
    /*}*/
</style>
<div class="px-3">

    {!!Form::open(['url' => route('doc.insert'), 'enctype' => 'multipart/form-data', 'id' => 'document_upload', 'class' => 'form form-horizontal form-bordered' ]) !!}

    <div class="form-body">

        <div class="form-group required row">
            <label class="col-md-3 label-control" for="eventRegInput1"> Upload Document: </label>
            <div class="col-md-9">
                {{--                    {!! Form::file('pic', null, array('class'=>'form-control input-lg', 'mulitple' => true)) !!}--}}
                <input type="file" name="pic[]" id="pic" value="pic" multiple="multiple" required>
                <span class="danger">  @if ($errors->has('pic')) {{ $errors->first('pic') }} @endif </span>

{{--                <span id="loadingMessage" style="visibility:hidden;">This page be uploading!</span>--}}
            </div>
{{--            <div class="progress">--}}
{{--                <div class="bar"></div >--}}
{{--                <div class="percent">0%</div >--}}
{{--            </div>--}}

        </div>
        <div class="form-group row">
            <div>
                <div class="col-md-12 form-group">
                    <button type="submit" class="btn btn-info btn-success submit_btn upload_file">Save</button>
                </div>
            </div>
        </div>
        <input id="fileUserId" name="user_id" type="hidden" >
    </div>
    {{ Form::close() }}
    <br />
    <div class="progress">
        <div class="progress-bar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div >
    </div>
    <div class="row" id="success"></div>

</div>
<script>
    // $(document).ready(function() {
    //     $('Form').ajaxForm({
    //         beforeSend:function(){
    //           $('#success').empty();
    //           $('.progress-bar').text('0%');
    //           $('.progress-bar').css('width','0')
    //         },
    //         uploadProgress:function(event, position, total, percentComplete) {
    //             $('.progress-bar').text(percentComplete + '0%');
    //             $('.progress-bar').css('width', percentComplete + '0%');
    //         },
    //         success:function(data){
    //             if(data.success){
    //                 $('#success').html('<div class="text-success text-center"><b>'+data.success+'</b></div><br /><br />')
    //             }
    //         }
    //     });
    // });
    // $("#pic").change(function(){
    //     swal.fire({
    //         title: "File uploading",
    //         button: false,
    //         timer: 3000
    //     })
    // });
{{--   $("#pic").change(function() {--}}
{{--        var bar = $('.bar');--}}
{{--        var percent = $('.percent');--}}
{{--        var status = $('#status');--}}
{{--        let url = `{{route('document.upload')}}`;--}}
{{--        $('form').ajaxForm({--}}
{{--            beforeSend: function() {--}}
{{--                status.empty();--}}
{{--                var percentVal = '0%';--}}
{{--                var posterValue = $('input[name=pic]').fieldValue();--}}
{{--                bar.width(percentVal)--}}
{{--                percent.html(percentVal);--}}
{{--            },--}}
{{--            uploadProgress: function(event, position, total, percentComplete) {--}}
{{--                var percentVal = percentComplete + '%';--}}
{{--                bar.width(percentVal)--}}
{{--                percent.html(percentVal);--}}
{{--            },--}}
{{--            success: function() {--}}
{{--                var percentVal = 'Wait, Saving';--}}
{{--                bar.width(percentVal)--}}
{{--                percent.html(percentVal);--}}
{{--            },--}}
{{--            complete: function(xhr) {--}}
{{--                status.html(xhr.responseText);--}}
{{--                alert('Uploaded Successfully');--}}
{{--                // window.location.href = "document.upload";--}}
{{--            }--}}
{{--        });--}}

{{--    })();--}}

</script>



