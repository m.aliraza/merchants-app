@extends('layouts.default')
@section('content')

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">

                <section id="striped-light">
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
                                <a href={{url('doc/add')}} style="color: white">Add New Document</a>
                            </button>
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">All Documents</h4>
                                    <hr>

                                    <div class="form-body row">

                                        <div class="col-md-2">
                                            <select class="form-control" name="bulk_action" id="action_doc">
                                                <option value="" selected> Select Status</option>
                                                <option value="active"> Active </option>
                                                <option value="inactive"> In Active</option>
                                            </select>
                                            <span role="alert" class="invalid-feedback bulk-action-error"><strong></strong></span>
                                        </div>

                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-info btn-warning submit_btn" style="color: white;" id="apply_doc"  data-url="{{ url('/doc/action') }}">Apply</button>
                                        </div>

                                    </div>

                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>
                                                    <div class="custom-control custom-checkbox m-0">
                                                        <input type="checkbox" class="custom-control-input" id="item-select-all">
                                                        <label class="custom-control-label" for="item-select-all"></label>
                                                    </div>
                                                </th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>View File</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($files as $file)
                                                <tr>
                                                    <td>
                                                        <div class="custom-control custom-checkbox m-0">
                                                            <input type="checkbox" class="custom-control-input checkbox" id="item{{$file->id}}" value="{{$file->id}}">
                                                            <label class="custom-control-label" for="item{{$file->id}}"></label>
                                                        </div>
                                                    </td>
                                                    <td> {{$file->title}}</td>
                                                    <td> {{$file->comment}}</td>
                                                    <td>{{$file->real_name}}</td>
                                                    <td>
                                                        @if($file->status == 1)
                                                            <a class="btn white btn-round btn-success">Active</a>
                                                        @elseif($file->status == 2)
                                                            <a class="btn white btn-round btn-danger">In-Active</a>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a target="_blank" href={{url('media/doc').'/'.$file->name}} class="btn white btn-round btn-primary">View Doc</a>
                                                    </td>

                                                    @endforeach

                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
