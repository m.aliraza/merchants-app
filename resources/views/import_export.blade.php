@extends('layouts.default')
@section('content')

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">

                <section id="striped-light">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="card">
                                @if(\Illuminate\Support\Facades\Session::has('info'))
                                    <div class="alert alert-success"> <span class="glyphicon glyphicon-ok"></span> <em style="color: white"> {!! session('info') !!}</em> </div>
                                @endif

                                <div class="card-header">
                                    <h4 class="card-title">Import & Export User Data</h4>
                                </div>

                                <hr>
                                <div class="card-content">
                                    <div class="card-body">

                                        <div class="form-actions">
                                            {!!Form::open(['url' => route('user.export'), 'enctype' => 'multipart/form-data', 'id' => 'user_form', 'class' => 'form' ]) !!}
                                            <input type="submit" name="name" class="btn btn-success" value="Export Users">
                                            {{ Form::close() }}
                                        </div>
                                        <hr>
                                        <div class="form-actions">
                                            {!!Form::open(['url' => route('user.import'), 'enctype' => 'multipart/form-data', 'id' => 'user_form', 'class' => 'form' ]) !!}
                                                <input type="file" name="excel_file" class="btn btn-success" value="import Users">
                                                <input type="submit" name="name" class="btn btn-success" value="Import Users">
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </section>

                <section id="striped-light">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="card">
                                @if(\Illuminate\Support\Facades\Session::has('info_app'))
                                    <div class="alert alert-success"> <span class="glyphicon glyphicon-ok"></span> <em style="color: white"> {!! session('info_app') !!}</em> </div>
                                @endif

                                <div class="card-header">
                                    <h4 class="card-title">Import & Export Application Submissions </h4>
                                </div>

                                <hr>
                                <div class="card-content">
                                    <div class="card-body">

                                        <div class="form-actions">
                                            {!!Form::open(['url' => route('app.export'), 'enctype' => 'multipart/form-data', 'id' => 'user_form', 'class' => 'form' ]) !!}
                                            <input type="submit" name="name" class="btn btn-success" value="Export Submissions">
                                            {{ Form::close() }}
                                        </div>
                                        <hr>
                                        <div class="form-actions">
                                            {!!Form::open(['url' => route('app.import'), 'enctype' => 'multipart/form-data', 'id' => 'user_form', 'class' => 'form' ]) !!}
                                            <input type="file" name="excel_file" class="btn btn-success" value="import Users">
                                            <input type="submit" name="name" class="btn btn-success" value="Import Submissions">
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
