<div class="all-folders-row d-flex">
    @foreach($folders as $directory)
        <div class="folder-block mr-5 folder">
            <i class="fa fa-minus-square-o delete-icon" aria-hidden="true"></i>
            <div class="open-folder folder-wrapper" data-name="{{$directory->name}}" data-id="{{$directory->id}}" >
                <img src="{{asset('images/folder.png')}}">
                <div class="edit-icon">
                    {{$directory->name}}<i class="edit-name fa fa-pencil ml-2" aria-hidden="true"></i>
                    @if($directory->user_id)
                        ({{$directory->user_id}})
                    @endif
                </div>
            </div>
            <input class="folder-id" type="hidden" value="{{$directory->id}}">
            <input class="folder_user_id" type="hidden" value="{{$directory->user_id}}">
        </div>
    @endforeach
    <input id="parentId" type="hidden" value="{{$root}}">
</div>
