<!doctype html>
<head>
    @include('includes.head')
    @yield('css')
</head>
<body>
<div id="wrapper">
    @include('includes.header')
    @include('includes.left_menu')
    @yield('content')
</div>

@yield('script')

</body>
</html>
