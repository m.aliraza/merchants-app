@extends('layouts.default')
@section('css')
    <style>
        .card-body {
            overflow-x: auto;
        }
        .action-wrapper {
            display: flex;
            align-items: center;
            margin-top: 10px;
        }
        .action-wrapper a {
            margin-right: 5px;
        }
    </style>
@endsection
@section('content')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">

                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="card gradient-blackberry" style=" border-radius: 24px; width: 18pc; height: 7pc; float: right">
                            <div class="card-content">
                                <div class="card-body pt-2 pb-0">
                                    <div class="media">
                                        <div class="media-body white text-center">
                                            <h3 class="font-large-1 mb-0" style="font-size: 40px !important; padding-bottom: 32px;" >{{ count($applications)  }}</h3>
                                            <span>Recent Merchant Applications</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="card gradient-ibiza-sunset" style=" border-radius: 24px; width: 18pc; height: 7pc; margin-left: 108px" >
                            <div class="card-content">
                                <div class="card-body pt-2 pb-0">
                                    <div class="media">
                                        <div class="media-body white text-center">
                                            <h3 class="font-large-1 mb-0" style="font-size: 40px !important; padding-bottom: 32px;">{{$active_applications}}</h3>
                                            <span>Open Merchant Applications</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart1" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2" style=" border-radius: 24px; width: 18pc; height: 7pc;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="card gradient-green-tea" style=" border-radius: 24px; width: 18pc; height: 7pc; float: left">
                            <div class="card-content">
                                <div class="card-body pt-2 pb-0">
                                    <div class="media">
                                        <div class="media-body white text-center">
                                            <h3 class="font-large-1 mb-0" style="font-size: 40px !important; padding-bottom: 32px;">{{$closed_applications}}</h3>
                                            <span>Closed Merchant Applications</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart2" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <section id="striped-light">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Recent Merchant Applications (Listings)</h4>
                                    <hr>
                                    {!!Form::open(['url' => route('home'), 'class' => 'form form-horizontal form-bordered' ,'method'=> 'GET']) !!}

                                    {!! Form::hidden('home_page', old('home_page','home_page'), array('class'=>'form-control input-lg',)) !!}

                                    <div class="form-body row">
                                        <div class="col-md-4">
                                            {!! Form::text('search_keyword', null, array('class'=>'form-control input-lg','placeholder'=>'Search')) !!}
                                        </div>

                                        <div class="col-md-2">
                                            <select class="form-control" name="status">
                                                <option value="" selected> All Status</option>
                                                {{--                                                    <option value= 1 {{ old('status') == 1 ? 'selected' : ''}}> Created</option>--}}
                                                <option value= 2 {{ old('status') == 2 ? 'selected': ''}}> Open </option>
                                                <option value= 3 {{ old('status') == 3 ? 'selected' : ''}}> Closed</option>
                                                <option value= 4 {{ old('status') == 4 ? 'selected' : ''}}> Pending</option>
                                            </select>
                                        </div>
					<div class="col-md-2">
                                                <select class="form-control" name="sort">
                                                    <option value="" {{ empty(Request::input('sort')) ? 'selected' : '' }} >Sort Order: </option>
                                                    <option value="asc" {{ Request::input('sort') == 'asc' ? 'selected' : '' }}>A-Z</option>
                                                    <option value="desc" {{ Request::input('sort') == 'desc' ? 'selected' : '' }}>Z-A</option>
                                                </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-info btn-success submit_btn">Filter</button>
                                        </div>
                                        {{ Form::close() }}

                                    </div>

                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>
                                                    <div class="custom-control custom-checkbox m-0">
                                                        <input type="checkbox" class="custom-control-input" id="item-select-all">
                                                        <label class="custom-control-label" for="item-select-all"></label>
                                                    </div>
                                                </th>
                                                <th>MID</th>
{{--                                                <th>Legal name</th>--}}
                                                <th>DBA</th>
                                                <th>Address</th>
                                                <th>City</th>
                                                <th>Province</th>
                                                <th>Country</th>
                                                <th>Contact</th>
                                                <th>Phone No</th>
                                                <th>Date Open</th>
                                                <th>Agent</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($applications as $application)
                                                <tr>
                                                    <td> <div class="custom-control custom-checkbox m-0">
                                                            <input type="checkbox" class="custom-control-input checkbox" id="item{{$application->id}}" value="{{$application->id}}">
                                                            <label class="custom-control-label" for="item{{$application->id}}"></label>
                                                        </div>
                                                    </td>
                                                    <td> {{$application->mid}}</td>
{{--                                                    <td>{{$application->legal_name}}</td>--}}
                                                    <td> {{$application->dba}}</td>
                                                    <td>{{$application->address_1}}</td>
                                                    <td>{{$application->city}}</td>
                                                    <td>{{$application->state}}</td>
                                                    <td>{{$application->country}}</td>
                                                    <td>{{$application->contact}}</td>
                                                    <td>{{$application->phone_no}}</td>
                                                    <td>{{$application->open_date}}</td>

                                                    <td>
                                                        @if(isset($application->user->first_name))
                                                            {{$application->user->first_name}} {{$application->user->last_name}}
                                                        @endif
                                                    </td>

                                                    <td>
                                                        @if($application->status == 2)
                                                            <a class="btn white btn-round btn-success">Open</a>
                                                        @elseif($application->status == 3)
                                                            <a class="btn white btn-round btn-danger">Closed</a>
                                                            @elseif($application->status == 4)
                                                            <a class="btn white btn-round btn-danger">Pending</a>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        <div class="action-wrapper">
                                                            @if(Auth::user()->role == 'admin')
                                                                <a href={{route('app.edit',$application->id)}} class="warning" data-original-title="" title="">
                                                                    <i class="ft-edit-2" style="font-size: 22px"></i>
                                                                </a>
                                                            @endif
                                                            <a href={{route('app').'/'.$application->id}} class="primary" data-original-title="" title="">
                                                                <i class="ft-eye" style="font-size: 22px"></i>
                                                            </a>
                                                            <a href="{{route('app').'/'.$application->id}}#comment-box" class="primary" data-original-title="" title="">
                                                                <i class="fa fa-comment-o" aria-hidden="true" style="font-size: 22px"></i>
                                                            </a>
                                                            <a href="{{route('doc')}}?app_id={{$application->id}}" class="primary" data-original-title="" title="">
                                                                <i class="fa fa-file" aria-hidden="true" style="font-size: 22px"></i>
                                                            </a>
                                                        </div>

                                                    </td>

                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>



                </div>
            </div>
        </div>
    </div>
@endsection
