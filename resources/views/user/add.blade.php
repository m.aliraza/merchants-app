@extends('layouts.default')
@section('content')

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">

                <div class="row justify-content-md-center">

                    @if(\Illuminate\Support\Facades\Session::has('info'))
                        <div class="alert alert-success"> <span class="glyphicon glyphicon-ok"></span> <em style="color: white"> {!! session('info') !!}</em> </div>
                    @endif

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="bordered-layout-card-center">Add new user</h4>
                            </div>
                            <div class="card-body">
                                <div class="px-3">

                                    {!!Form::open(['url' => route('user.insert'), 'enctype' => 'multipart/form-data', 'id' => 'user_form', 'class' => 'form form-horizontal form-bordered' ]) !!}

                                        <div class="form-body">
                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="eventRegInput1"> First Name: </label>
                                                <div class="col-md-9">
                                                    {!! Form::text('first_name', null, array('class'=>'form-control input-lg',)) !!}
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="eventRegInput1"> Last Name: </label>
                                                <div class="col-md-9">
                                                    {!! Form::text('last_name', null, array('class'=>'form-control input-lg',)) !!}
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="eventRegInput1"> Company Name: </label>
                                                <div class="col-md-9">
                                                    {!! Form::text('display_name', null, array('class'=>'form-control input-lg',)) !!}
                                                    <span class="danger"> @if ($errors->has('display_name')) {{ $errors->first('display_name') }} @endif </span>
                                                </div>
                                            </div>

{{--                                            <div class="form-group row">--}}
{{--                                                <label class="col-md-3 label-control" for="eventRegInput1"> Company Name:</label>--}}
{{--                                                <div class="col-md-9">--}}
{{--                                                    {!! Form::text('company_name', null, array('class'=>'form-control input-lg',)) !!}--}}
{{--                                                    <span class="danger"> @if ($errors->has('company_name')) {{ $errors->first('company_name') }} @endif </span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            {!! Form::hidden('company_name', 111, array('class'=>'form-control input-lg',)) !!}

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="eventRegInput1"> Telephone:</label>
                                                <div class="col-md-9">
                                                    {!! Form::text('phone_no', null, array('class'=>'form-control input-lg',)) !!}
                                                    <span class="danger"> @if ($errors->has('phone_no')) {{ $errors->first('phone_no') }} @endif </span>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="eventRegInput1"> E-mail: </label>

                                                <div class="col-md-9">
                                                    {!! Form::email('email', null, array('class'=>'form-control input-lg',)) !!}
{{--                                                    <span class="danger"> @if ($errors->has('email')) {{ $errors->first('email') }} @endif </span>--}}
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="eventRegInput1"> User Type:</label>
                                                <div class="col-md-9">
                                                    <select id="user_role" class="form-control" name="role">
                                                        <option value="" selected> select</option>
                                                        <option value= admin {{ old('role') == 'admin' ? 'selected' : ''}}> Admin </option>
                                                        <option value= super_user{{ old('role') == 'super_user' ? 'selected': ''}}> Super User</option>
                                                        <option value= user {{ old('role') == 'user' ? 'selected' : ''}}> User </option>
                                                    </select>
                                                </div>
                                            </div>

					    <div id="supervisors" class="form-group row" style="display: none;">
                                               <label class="col-md-3 label-control">Users for Supervision:</label>
						<div class="col-md-9">
                                                  <select class="form-control required" multiple name="user_ids[]">
                                                    @foreach($users as $user)
                                                     <option value={{$user->id}}> {{$user->first_name}} &nbsp; {{ $user->last_name }} </option>
                                                    @endforeach
                                                 </select>
					        </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="eventRegInput1"> Password:</label>
                                                <div class="col-md-9">
                                                    {!! Form::text('password', null, array('class'=>'form-control input-lg',)) !!}
                                                    <span class="danger"> @if ($errors->has('password')) {{ $errors->first('pass    word') }} @endif </span>
                                                </div>
                                            </div>





                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="eventRegInput1"> Upload Profile Picture: </label>
                                                <div class="col-md-9">
                                                    {!! Form::file('pic', null, array('class'=>'form-control input-lg',)) !!}
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div>
                                                    <div class="col-md-12 form-group">
                                                        <button type="submit" class="btn btn-info btn-success submit_btn">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <script>
        $("#user_role").change(function(){
            if ($("#user_role").val() == 'super_user') {
		$("#supervisors").show();
	    } else {
		$("#supervisors").hide();
	    }
        });
    </script>
@endsection
