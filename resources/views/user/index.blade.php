﻿@extends('layouts.default')
@section('content')

    @include('user.email-modal')

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                @if(Session::has('success'))
                    <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('success') }}</strong>
                    </div>
                @endif
                <section id="striped-light">
                    @if(\Illuminate\Support\Facades\Session::has('info'))
                        <div class="alert alert-success"> <span class="glyphicon glyphicon-ok"></span> <em style="color: white"> {!! session('info') !!}</em> </div>
                    @endif
                    <div class="row">
                            <div class="col-sm-12">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
                                <a href={{route('user.add')}} style="color: white;">Add New User</a>
                            </button>
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">All users</h4>
                                    <hr>
                                    {!!Form::open(['url' => route('user'), 'class' => 'form form-horizontal form-bordered' ,'method'=> 'GET']) !!}

                                    <div class="form-body row">
                                            <div class="col-md-2">
                                                {!! Form::text('search_keyword', null, array('class'=>'form-control input-lg','placeholder'=>'Search')) !!}
                                            </div>

                                            <div class="col-md-2">
                                                <select class="form-control" name="role">
                                                    <option value="" selected> Select User Type: </option>
                                                    <option value= admin {{ old('role') == 'admin' ? 'selected' : ''}}> Admin </option>
                                                    <option value= super_user {{ old('role') == 'super_user' ? 'selected': ''}}> Super User</option>
                                                    <option value= user {{ old('role') == 'user' ? 'selected' : ''}}> User </option>
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <select class="form-control" name="status">
                                                    <option value="" selected> All Status</option>
{{--                                                    <option value= 1 {{ old('status') == 1 ? 'selected' : ''}}> Created</option>--}}
                                                    <option value= 2 {{ old('status') == 2 ? 'selected': ''}}> Active </option>
                                                    <option value= 3 {{ old('status') == 3 ? 'selected' : ''}}> Inactive</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-info btn-success submit_btn">Filter Results</button>
                                            </div>
                                        {{ Form::close() }}

                                        <div class="col-md-2">
                                            <select class="form-control" name="bulk_action" id="action">
                                                <option value="" selected> All </option>
{{--                                                <option value="created"> Created</option>--}}
                                                <option value="active"> Active </option>
                                                <option value="inactive"> Inactive</option>
                                                <option value="delete"> Delete </option>
                                                <option value="send_email"> Send Email</option>
                                            </select>
                                            <span role="alert" class="invalid-feedback bulk-action-error"><strong></strong></span>
                                        </div>

                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-info btn-warning submit_btn" style="color: white;" id="apply"  data-url="{{ url('/users/action') }}">Apply</button>
                                        </div>

                                    </div>

                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>
                                                    <div class="custom-control custom-checkbox m-0">
                                                        <input type="checkbox" class="custom-control-input" id="item-select-all">
                                                        <label class="custom-control-label" for="item-select-all"></label>
                                                    </div>
                                                </th>
					        <th>ID</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Company Name</th>
                                                <th>Telephone</th>
                                                <th>E-mail</th>
                                                <th>User Type</th>
                                                <th>Status</th>
                                                <th>Actions</th>
{{--                                                <th><input type="checkbox" class="check_all"></th>--}}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {!!Form::open(['url' => route('user.change_status'), 'class' => 'form form-horizontal form-bordered']) !!}

                                            @foreach($users as $user)
                                                <tr>
                                                    <td>
                                                        <div class="custom-control custom-checkbox m-0">
                                                            <input type="checkbox" class="custom-control-input checkbox" id="item{{$user->id}}" value="{{$user->id}}">
                                                            <label class="custom-control-label" for="item{{$user->id}}"></label>
                                                        </div>
                                                    </td>
						    <td> {{$user->id}}</td>
                                                    <td> {{$user->first_name}}</td>
                                                    <td> {{$user->last_name}}</td>
                                                    <td>{{$user->display_name}}</td>
                                                    <td>{{$user->phone_no}}</td>
                                                    <td>{{$user->email}}</td>
                                                    <td>
                                                        @if($user->role == 'admin')
                                                            <a class="btn white btn-round btn-danger">Admin</a>
                                                        @elseif($user->role == 'super_user')
                                                            <a class="btn white btn-round btn-warning">Super User</a>
                                                        @elseif($user->role == 'user')
                                                            <a class="btn white btn-round btn-primary">User</a>
                                                        @endif</td>
                                                    <td>
                                                        @if($user->status == 2)
                                                            <a class="btn white btn-round btn-success">Active</a>
                                                        @elseif($user->status == 3)
                                                            <a class="btn white btn-round btn-danger">Inactive</a>
                                                            @endif
                                                    </td>
                                                    <td>
                                                        <a href={{route('user.edit',$user->id)}} class="warning" data-original-title="" title="">
                                                            <i class="ft-edit-2" style="font-size: 22px"></i>
                                                        </a>
                                                        <a href={{route('user').'/'.$user->id}} class="primary" data-original-title="" title="">
                                                            <i class="ft-eye" style="font-size: 22px"></i>
                                                        </a>
                                                    </td>
{{--                                                    <td><input type="checkbox" name="check_ids[]" value={{$user->id}} class='check_single'></td>--}}

                                                </tr>
                                            @endforeach
                                            {{ Form::close() }}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").click(function(){
            $(".check_single").prop("checked",$(this).prop("checked"));
        });
    </script>
@endsection
