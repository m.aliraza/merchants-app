@extends('layouts.default')
@section('content')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                <!--About section starts-->
                <section id="about">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5> <b>{{$user_object->display_name}}</b></h5>
                                </div>
                                <hr>
                                <div class="row">
                                    @if(!empty($user_object->media_object->name))
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <img src={{url('media/user').'/'.$user_object->media_object->name}} alt="user_image" width="300" align="center" style="border-radius: 20px; margin-left: 90px;">
                                    </div>
                                    @endif
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <ul class="no-list-style">
                                            <li class="mb-2">
                                                <span class="text-bold-500 primary"><a><i class="icon-user font-small-3"></i> First Name</a></span>
                                                <span class="d-block overflow-hidden">{{$user_object->first_name}}</span>
                                            </li>
                                            <li class="mb-2">
                                                <span class="text-bold-500 primary"><a><i class="icon-user font-small-3"></i> Last Name</a></span>
                                                <span class="d-block overflow-hidden">{{$user_object->last_name}}</span>
                                            </li>
                                            <li class="mb-2">
                                                <span class="text-bold-500 primary"><a><i class="icon-user font-small-3"></i> Contact Name</a></span>
                                                <span class="d-block overflow-hidden">{{$user_object->display_name}}</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <ul class="no-list-style">
{{--                                            <li class="mb-2">--}}
{{--                                                <span class="text-bold-500 primary"><a><i class="ft-user font-small-3"></i> Company Name</a></span>--}}
{{--                                                <span class="d-block overflow-hidden">{{$user_object->display_name}}</span>--}}
{{--                                            </li>--}}

                                            <li class="mb-2">
                                                <span class="text-bold-500 primary"><a><i class="ft-monitor font-small-3"></i> Telephone No:</a></span>
                                                <a class="d-block overflow-hidden">{{$user_object->phone_no}}</a>
                                            </li>
                                            <li class="mb-2">
                                                <span class="text-bold-500 primary"><a><i class="ft-mail font-small-3"></i> E-mail:</a></span>
                                                <a class="d-block overflow-hidden">{{$user_object->email}}</a>
                                            </li>

                                            <li class="mb-2">
                                                @if($user_object->status == 2)
                                                    <a class="btn white btn-round btn-success">Active</a>
                                                @elseif($user_object->status == 3)
                                                    <a class="btn white btn-round btn-warning">Inactive</a>
                                                @endif
                                            </li>

                                            <li>
                                                <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
                                                    <a href={{route('user.edit',$user_object->id)}}  style="color: white;">Edit User Info</a>
                                                </button>
                                            </li>

                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection