<!-- Modal -->
<div class="modal @if(Session::has('showModal')) @else fade @endif" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" @if(Session::has('showModal')) style="display:block" @endif>
    {!!Form::open(['url' => route('send.bulk.emails'), 'class' => 'form form-horizontal form-bordered' ,'method'=> 'POST', 'id' => 'sendBulkEmails', 'novalidate' => 'novalidate']) !!}
    {!! Form::hidden('users', old('users'), ['id' => 'users_ids_field']) !!}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group row">
                    <label for="Email subject">Subject</label>
                    @if(Session::has('showModal'))
                        <span style="font-size:14px; color:#ff0c27">Message field is required</span>
                    @endif
                    <input type="text" name="email_subject" required style="width: 800px">
                </div>


                <div class="form-group">
                    <label for="Email Body">Message</label>
                    @if(Session::has('showModal'))
                        <span style="font-size:14px; color:#ff0c27">Message field is required</span>
                    @endif

                    <textarea id="editor" rows="5" cols="10" required name="message">{{ old('message') }}</textarea>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Send E-mail</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@if(Session::has('showModal'))
<div class="modal-backdrop @if(Session::has('showModal')) fade show @endif  "></div>
@endif
